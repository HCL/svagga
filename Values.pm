package Values;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings;

sub round {
    my ($value, $round) = @_;
    $value = sprintf ("%0.".$round."f", $value);
    $value = supressUselessZero($value);
    return($value);
}

sub supressUselessZero {
    my ($value) = @_;
    while($value =~ m/0$/){
        $value =~ s/0$//;
    }
    $value =~ s/\.$//;
    return($value);
}
1;