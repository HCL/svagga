package Fields;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => "all";

our %understood_fields = (
	"Sample" => 1,
    # Breakpoint
	"Breakpoint" => 1,
    "Breakpoint_Instance" => 1, # Programmer
    "Chromosome" => 1,
    "Position" => 1,
    "Start" => 1,
    "End" => 1,
    "Length" => 1,
    "Orientation" => 1,
    "Occurrence" => 1,
    "Order_In_Call" => 1,
    # Call
    "Call_Length" => 1,
    "Call_Occurrence" => 1,
    "Link_Occurrence" => 1,
    # Calls and callers
    "Event" => 1,
    "Caller" => 1,
    "Caller_Occurrence" => 1,
    # "Caller_Version" => 1,
    # "Caller_Method" => 1,
    # Complex events (linked translocations, etc.)
    "Complex_Event_ID" => 1,
    # Annotations
	"Gene" => 1,
);

# Field instance
sub new {
	my ($class, $basic_field_name) = @_;
	$class = ref($class) || $class;
	my $field = {};
	$field->{"basic_track"} = $basic_field_name;
	$field->{"understood_track"} = undef;
	$field->{"source_type"} = undef;
	$field->{"source_file"} = undef;
	$field->{"mate"} = undef;
	bless($field, $class);
	return($field);
}

# UTILISE
sub createNewField {
    my ($field_name) = @_;
    my $field = Fields->new($field_name);
    $field->interpreteName();
    return $field;
}

# UTILISE
sub interpreteName {
    my ($field) = @_;
    $field->determinePrefixesAndSuffixes();
    $field->determineIfBasic();
    $field->setPreciseUnderstoodTrack();
}

# UTILISE
sub determineIfBasic {
    my ($field) = @_;
    if ($field->{"interpreted_track"}) {
        my $known_track = isBasicField($field->{"interpreted_track"});
        if ($known_track) {
            $field->{"interpreted_track"} = $known_track;
		    $field->{"source_type"}{"basicTrack"} = $known_track;
        }
    } else {
        die Errors::ProgrammerBug("Impossible to determine is a track is known with none interpreted_track");
    }
}

sub setPreciseUnderstoodTrack {
    my ($field) = @_;
    if ($field ->{mate}) {
        $field->{"precise_interpreted_track"} = "Mate_";
    }
    if ($field->{"interpreted_track"}) {
        $field->{"precise_interpreted_track"} .= $field->{"interpreted_track"};
    } else {
        die Errors::ProgrammerBug("Impossible to set the precise field track with none interpreted_track");
    }
}

# UTILISE
sub determinePrefixesAndSuffixes {
    my ($field) = @_;
    # We do not alter the basic track, but the interpreted track sees its prefixes/suffixes removed
    $field->{"interpreted_track"} = $field->{"basic_track"};
    if ($field->{"interpreted_track"} =~ s/^Mate_//) {
        $field->{mate} = 1;
    }
}

# UTILISE ?
sub knownField {
    my ($field) = @_;
    $field = removePrefixesAndSuffixes($field);
    return (isBasicField($field));
}

# UTILISE ?
sub removePrefixesAndSuffixes {
    my ($field) = @_;
    $field =~ s/^Mate_//;
    return ($field);
}


# UTILISE
# Function which compares a given field to any understood fields
sub isBasicField {
    my ($field) = @_;
    # It removes a putative final "s"
    my $temp_field = $field;
    $temp_field =~ s/s$//;
    foreach my $understood_field (keys %understood_fields) {
        if (lc($temp_field) eq lc($understood_field)) {
            return($understood_field);
        }
    }
    # If none field corresponds, we return 0
    return(0);
}

# UTILISE
sub setTrack {
		my ($field, $track, $option_hash, $data_hash) = @_;
		$field->{basic_track} = $track; # This will be useful for alert/error messages
        $field->interpreteName();
}



1;