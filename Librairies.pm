package Librairies;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => "all";

# Other libraries
use Switch;
use List::Util qw(min max sum);
use Scalar::Util qw(looks_like_number);
use POSIX;

# use Bio::Tools::GFF; TODO
# Svagga libraries
use Version;
use Arguments;
use Options;
use Errors;

# Generalities
use BasicFunctions;
use Values;
use Fields;
use Samples;

# Inputs
# use VariantCaller;
use Files;
use VCF;
use Genomes;
# Configuration
use Objects;
use Configuration;
use PrintedFields;
use AnnotationFiles;
# Genes
use Genes;
use Transcripts;
use Exons;

# Intervals
use Intervals;
use BreakPoints;
# Links
use Links;
use Calls;
use Events;
use BED;

# Outputs
use Outputs;
use Impression;
use Folders;

1;