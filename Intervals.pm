package Intervals;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

# Interval Object, which contains a chromosome position, a start and an end
sub new {
	my ($class, $chromosome, $start, $end) = @_;
	$class = ref($class) || $class;
	my $interval = {};
	bless($interval, $class);
	 # An interval instance contain two mandatory positions, but the second one can be identical to the first
	$interval->{"start"} = $start;
	$interval->{"end"} = $end ? $end : $start;
	if (defined($chromosome)) {
		$interval->{"chromosome"} = $chromosome;
	} else {
		die (Errors::ProgrammerBug("Chromosome not defined in Interval element.\nStart: $start.\nEnd : $end.\n"));
	}
	return($interval);
}

# UTILISE
# Function which stores an interval into an array, by its first boundary, and regroup events
sub storeAndSortOnChromosomeArray {
	my ($interval, $array) = @_;
	my $index_nb = 0;
	foreach my $compared_interval (@$array) {
		# The bp index increases until the next element has a superior start...
		if ($compared_interval->{"start"} > $interval->{"start"}) {
			last;
		# Or the same start but a superior end
		} elsif ($compared_interval->{"start"} == $interval->{"start"} && $compared_interval->{"end"} > $interval->{"end"}) {
			last;
		}
		$index_nb++;
	}
	splice @$array, $index_nb, 0, $interval;
}

# UTILISE
sub toSimpleIntervalString {
	my ($breakpoint_instance) = @_;
	return $breakpoint_instance->{"chromosome"}.":".$breakpoint_instance->{"start"}."-".$breakpoint_instance->{"end"};
}

# UTILISE
sub enlargeBoundaries {
	my ($interval, $tested_5p_boundary, $tested_3p_boundary) = @_;
	if ($interval->{"start"} >  $tested_5p_boundary) {
		$interval->{"start"} = $tested_5p_boundary;
	}
	if ($interval->{"end"} <  $tested_3p_boundary) {
		$interval->{"end"} = $tested_3p_boundary;
	}
}

# UTILISE
sub updateTrackAnnotation {
	my ($interval, $track, $info, $distance) = @_;
	# We store all the overlapping information
	if ($distance == 0) {
		$interval->{"annotations"}{$track}{"overlapping"}{$info} = $info;
	} else {
		# Or the close information
		my $before_or_after;
		if ($distance > 0) {
			$before_or_after = "before";
		} else {
			$before_or_after = "after";
		}
		# We only keep the absolute value for convenience
		$distance = abs($distance);
		my $save;
		if (defined($interval->{"annotations"}{$track}{"distance_".$before_or_after})) {
			if ($distance < $interval->{"annotations"}{$track}{"distance_".$before_or_after}) {
				undef %{ $interval->{"annotations"}{$track}{$before_or_after} };
				$save = 1;
			} elsif ($distance == $interval->{"annotations"}{$track}{"distance_".$before_or_after}) {
				$save = 1;
			}
		} else {
			$save = 1;
		}
		# We both save the info and the new distance
		if ($save) {
			$interval->{"annotations"}{$track}{$before_or_after}{$info} = $info;
			$interval->{"annotations"}{$track}{"distance_".$before_or_after} = $distance;
		}
	}

}

sub sortIntervalArray {
	my @intervals = @_;
	my @sorted_intervals;

	foreach my $target_intervals (@intervals) {

		my $position = 0;
		# Each bp is stored at its correct position, based on the bp already positionned
		CBP: foreach my $compared_intervals (@sorted_intervals)  {
			print $compared_intervals."\n";
			if ($target_intervals->isAfter($compared_intervals)) {
				$position++;
			} else {
				#last CBP;
			}
		}
		splice @sorted_intervals, $position, 0, $target_intervals;
	}

	return(@sorted_intervals);
}

sub distanceTo {
	my ($i1, $i2) = @_;
	return (distanceBetweenIntervals($i1->{"start"}, $i1->{"end"}, $i2->{"start"}, $i2->{"end"}));
	
}

sub isAfter {
	my $target_interval = $_[0];
	my $compared_interval = $_[1];

	die();

	# Be careful: to gain some time, the chromosome is not tested # TODO : think about this ?
	# The targeted interval is after the compared one...	
	# If its first position is after..
	if ($target_interval->{"start"} > $compared_interval->{"start"}) {
		return(1);
	# Or, if they are identical, and..
	} elsif ($target_interval->{"start"} == $compared_interval->{"start"}) {
		# Its second boundary is after
		if ($target_interval->{"start"} > $compared_interval->{"start"}) {
			return(1);
		}
	}
	return(0);
}

sub overlap {
	my ($interval, $compared_interval) = @_;
	print $interval->{"start"}."\t".$interval->{"end"}."\t".$compared_interval->{"start"}."\t".$compared_interval->{"end"}."\n";
	return(Intervals::areOverlapingIntervals($interval->{"start"}, $interval->{"end"}, $compared_interval->{"start"}, $compared_interval->{"end"}));
}

# Function whichs takes the boundaries of two BED regions, and returns "yes" is the two regions overlap
sub areOverlapingIntervals {
	my $interval_1_pos_1 = $_[0]; # The two first boundaries are the targetted one 
	my $interval_1_pos_2 = $_[1];
	my $interval_2_pos_1 = $_[2];
	my $interval_2_pos_2 = $_[3];

	if ($interval_1_pos_1 < $interval_2_pos_1 && $interval_1_pos_2 < $interval_2_pos_1) { # If the two boundaries of the first region are strictly before the second one, the two regions are not overlapping
		return(0);
	} elsif ($interval_1_pos_1 > $interval_2_pos_1 && $interval_1_pos_1 > $interval_2_pos_2) { # If the two boundaries of the first region are strictly after the second one, the two regions are not overlapping
		return(0);
	} else { # Else, the two regions are necessarily overlapping
		return(1)
	}
}


# Function whichs takes the boundaries of two BED regions, and returns the distanec of the second one from the first one
sub distanceBetweenIntervals {
	my $interval_1_pos_1 = $_[0]; # The two first boundaries are the targetted one 
	my $interval_1_pos_2 = $_[1];
	my $interval_2_pos_1 = $_[2];
	my $interval_2_pos_2 = $_[3];
	my $distance;
	if ($interval_1_pos_1 < $interval_2_pos_1 && $interval_1_pos_2 < $interval_2_pos_1) { # If the two boundaries of the first region are strictly before the second one, the two regions are not overlapping
		$distance = -1 * ($interval_2_pos_1 - $interval_1_pos_2);
	} elsif ($interval_1_pos_1 > $interval_2_pos_1 && $interval_1_pos_1 > $interval_2_pos_2) { # If the two boundaries of the first region are strictly after the second one, the two regions are not overlapping
		$distance = $interval_1_pos_1 - $interval_2_pos_2;
	} else { # Else, the two regions are necessarily overlapping
		$distance = 0;
	}
	return($distance);
}


# Function whichs takes one position and the boundaries of a BED region, and returns "yes" it is contained into the BED region
sub isThisPositionContainedIntoIntervals {
	my $position = $_[0]; # The two first boundaries are the targetted one 
	my $interval_pos_1 = $_[1];
	my $interval_pos_2 = $_[2];
	if ($position >= $interval_pos_1 && $position <= $interval_pos_2) {
		return(1);
	} else {
		return(0)
	}
}

# Function which gets BED positions and associated annotations from BED files # TODO : check if not obsolete
sub getBedPositionsFromFile {
	my $bed_hash = $_[0];
	my $bed_file = $_[1];
	open(my $bed_fh, "<", $bed_file) or die(Errors::formatFileOpeningError("BED"));
	#$bed_file =~ s/\.bed$//; # TODO : In a conf file, a name can be associated to the BED file to be added in the colomn - it will be a kind of title for the BED file and will replace the BED file name
	foreach my $bed_position (<$bed_fh>) {
		my @bed_position_info = split("\t", $bed_position);
		# 0-based BED positions are 1-based
		$bed_position_info[1]++;
		$bed_position_info[2]++;
		push @{ $$bed_hash{"Chromosomes"}{$bed_position_info[0]}{"BED"}{$bed_file} }, \@bed_position_info; # Annotation are stored as an array of arrays
	}
	close($bed_fh);
}

1;
