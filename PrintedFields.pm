package PrintedFields;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

use Switch;

# A PrintedFields is a Fields
our @ISA = ("Fields");

our %field_attributes = (
	"name" => 1, # The name of the field/column
	"track" => 1, # The source track
	"target" => { # The target in the source : closest element ? crossing elements ?
		"overlapping" => 1,
		"closest" => 1,
		"before" => 1,
		"after" => 1,
	},
	"info" => { # The printed information : the info field ? A 1/0 information ? The content from another track ?
		"content" => 1,
		"yes_no" => 1,
	},
	"compute" => {	# Has the information to be computed ? (for example, do we take the mean of the read number for the different callers ?)
		"none" => 1,
		"mean" => 1,
	},
	"focus" => {
		"omim" => 1,
	},
);


# PrintedFields instance
sub new {
	my ($class) = @_;
	$class = ref($class) || $class;
	my $printedField = {};
	$printedField->{"target"} = "overlapping"; 
	$printedField->{"info"} = "content";
	$printedField->{"source_type"} = undef;
	$printedField->{"compute"} = "none";
	bless($printedField, $class);
	return($printedField);
}


sub getPrintedFields {
	my ($option_hash, $data_hash) = @_;
	print "## Configuration of the printed fields ##\n";
	foreach my $field_file (@{ $$option_hash{files}{"printed_field_configuration"} }) {
		print "- File : ".$field_file."\n";
		# Extraction of the desired fields
		@{ $$data_hash{"basic_fields"} } = Configuration::getConfigurationFileElements($field_file);
	}
	print "\n";
}

sub testPrintedFieldConsistency {
	my ($option_hash, $data_hash) = @_;
	print "## Determination of the fields to be printed ##\n";
	# Creation of printedField objects
	foreach my $printedField (@{ $$data_hash{"basic_fields"} }) {
		Configuration::createObject($printedField, "printedField", $option_hash, $data_hash);
	}
	print "\n";
}

# UTILISE
sub determineKey {
	my ($key) = @_;
	# Some approximations/synonyms are tolerated
	switch (lc($key)) {
		case %field_attributes {
			$key = $key;
		}
		case /field/ {
			$key = "name";
		}
		case /column/ {
			$key = "name";
		}
		case /source/ {
			$key = "track";
		}
		case /file/ {
			$key = "track";
		}
		case /bed/ {
			$key = "track";
		}
		case /info/ {
			$key = "info"; # TODO : allow content from another track
		} else {
			$key = 0;
		}
	}
	return($key);
}

# Basic functions
sub setValue {
	my ($printedField, $key, $value, $option_hash, $data_hash) = @_;
	#We guess the field attribute (name, track, etc.) 
	my $interpreted_key = determineKey($key);
	if (!($interpreted_key)) {
		die("Unknown option in the annotation file configuration : $key\n");
	}
	# Same for the values of the track key, which is a special key
	if ($interpreted_key eq "track") {
		$printedField->setTrack($value, $option_hash, $data_hash);
	} elsif ($interpreted_key eq "target") {
		$printedField->setTargetValue($value, $option_hash, $data_hash);
	} elsif  ($interpreted_key eq "info") {
		$printedField->setInfoValue($value, $option_hash, $data_hash);
	} elsif ($interpreted_key eq "compute") {
		$printedField->setComputeValue($value, $option_hash, $data_hash);
	} elsif ($interpreted_key eq "focus") {
		$printedField->setFocusValue($value, $option_hash, $data_hash);
	} else {
		$printedField->{$interpreted_key} = $value;
	}

}



sub setTargetValue {
	my ($printedField, $value, $option_hash, $data_hash) = @_;
	if ($field_attributes{"target"}{$value}) {
		$printedField->{target} = $value;
	} else {
		die("Incorrect \"target\" value : $value\n");
	}
}

sub setFocusValue {
	my ($printedField, $value, $option_hash, $data_hash) = @_;
	if ($field_attributes{"focus"}{$value}) {
		$printedField->{focus} = $value;
	} else {
		die("Incorrect \"focus\" value : $value\n");
	}
}

sub setInfoValue {
	my ($printedField, $value, $option_hash, $data_hash) = @_;
	if ($field_attributes{"info"}{$value}) {
		$printedField->{info} = $value;
	} else {
		die("Incorrect \"info\" value : $value\n");
	}
}

sub setComputeValue {
	my ($printedField, $value, $option_hash, $data_hash) = @_;
	if ($field_attributes{"compute"}{$value}) {
		$printedField->{compute} = $value;
	} else {
		die("Incorrect \"compute\" value : $value\n");
	}
}

sub isValid {
	my ($printedField, $data_hash) = @_;
	# Track is mandatory for a printed file
	if (!(defined($printedField->{interpreted_track}))) {
		die("Track/source information is mandatory for a printed field :\n".Objects::enumerate($printedField)."\n");
	}

	# The printed field has to be understood, whatever his source is
	if (!(sourceIsDetermined($printedField, $data_hash))) {
		die("The source track ".$printedField->{interpreted_track}." cannot be determined for the printed field :\n".Objects::enumerate($printedField)."\nPlease provide it in the caller or annotation configuration files.\n");
	}

	#  If no "name" is given for the colomn, the field takes the "track" name
	if (!(defined($printedField->{"name"}))) {
		$printedField->{"name"} = $printedField->{interpreted_track};
	}
	return(1);
}

#  UTILISE
sub sourceIsDetermined {
	my ($printedField, $data_hash) = @_;
	my $understood = 0;
	my $is_basic_field = 0;
	my $is_annotation_field = 0;
	my $is_caller_field = 0;
	my $track = $printedField->{interpreted_track};
	# We first check in basic fields
	if (defined($Fields::understood_fields{$track})) {
		$printedField->{"source_type"}{"basicTrack"} = $track;
		$is_basic_field = 1;
		$understood = 1;
	}
	# We then check in the annotation fields
	if ($$data_hash{"files"}{"annotations"}{$track}) {
		if ($is_basic_field) {
			print Errors::Warnings()."The ".$printedField->{basic_track}." track present in the annotation call file is a basic Svagga field and will not been taken into account.\n".Errors::Documentation();
		}
		$is_annotation_field = 1;
		$understood = 1;
		$printedField->{"source_type"}{"annotationFile"} = $$data_hash{"files"}{"annotations"}{$track};
		$$data_hash{"tracks_of_interest"}{"annotationFiles"}{$track} = $$data_hash{"files"}{"annotations"}{$track};
	}
	# We then check in the call fields
	foreach my $caller (keys %{ $$data_hash{variant_callers} }) {
		# All the basic fields of VCF are first check
		if ($VCF::mandatoryColomns{$track} && !($track eq "FORMAT" || $track eq "INFO")) {
			$is_caller_field = 1;
			$understood = 1;
			$printedField->{"source_type"}{"callerField"}{$caller} = $$data_hash{variant_callers}{$caller};
		} else {
			foreach my $field (keys %{ $$data_hash{variant_callers}{$caller}->{fields} }) {
				if ($track eq $field) {
					# if ($is_basic_field || $is_annotation_field) {

					# }
					$is_caller_field = 1;
					$understood = 1;
					$printedField->{"source_type"}{"callerField"}{$caller} = $$data_hash{variant_callers}{$caller};
				}
			}
		}
	}
	# If a field is both a caller feld and basic/annotation fields, we print a warnings (TODO : a strict mode which generates die() in such cases)
	if ($is_caller_field) {
		if ($is_basic_field) {
			print Errors::Warnings()."The ".$printedField->{basic_track}." track present in the annotation call file is a basic Svagga field and will not been taken into account.\n".Errors::Documentation();
		}
	}
	return($understood);
}

#  UTILISE
sub store {
	my ($printedField, $data_hash) = @_;
	# The order will allow to print the colomn is the wished order
	push @{ $$data_hash{"sorted_fields"} }, $printedField;
	# The by-key declaration will allow to know that a field has already been declared
	$$data_hash{"declared_fields"}{ $printedField->{interpreted_track} } = 1;
}


1;
