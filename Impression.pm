package Impression;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings;

# Impression Object
sub new {
	my ($class, $output, $entities, $cluster) = @_;
	$class = ref($class) || $class;
	my $impression = {};
	bless($impression, $class);
	$impression-> {"output"} = $output; # File instance that will contain the impression
	$impression -> {"entities"} = $entities;
	$impression -> {"cluster"} = $cluster;
	$impression -> {"input_key"} = undef; # Will contain the input (sample or file) name 

	return($impression);
}

# UTILISE
# Function which prints the annotated breakpoints into the corresponding file(s)
sub printFiles {

	my $option_hash = $_[0];
	my $data_hash = $_[1];
	my $genes = $_[2];

	print "## Impression of the annotated breakpoints/events ###\n";
	foreach my $impression (@{ $$data_hash{"impressions"}{"annotatedBreakpoints"} }) {
		print "- ".$impression->{output}->{name}."\n";
		$impression -> printBreakpoints($option_hash, $data_hash);
	}
	print "\n";
}

# UTILISE
sub printBreakpoints {
	my ($impression, $option_hash, $data_hash) = @_;
	my $tabulatedString;
	my $breakpoint_array;

	# The printed entity will defined the hash where to find the data
	my $entity_key = $impression -> {"entities"};

	# We determine all the breakpoints that have to be printed
	my @breakpoints; 
	if ($impression -> {"cluster"} eq "by_sample") {
	# We can get them by sample
		my $input_sample = $impression -> {"input_key"};
		foreach my $chromosome (@Genomes::ordered_chromosomes) {
			if (!(defined($$data_hash{"samples"}{$input_sample}{$entity_key}{"chromosomes"}{$chromosome}[0]))) {
				next;
			}
			push @breakpoints, @{ $$data_hash{"samples"}{$input_sample}{$entity_key}{"chromosomes"}{$chromosome} };
		}
	} elsif ($impression -> {"cluster"} eq "by_file") {
	# We can get them by file
		my $input_file = $impression -> {"input_key"};
		push @breakpoints, @{ $$data_hash{"files"}{$entity_key}{$input_file} };
	} else {
		die(Errors::ProgrammerBug("This kind of cluster for the data output is not known.\n"));
	}

	# Impression
	# Output opening
	my $output_file = $impression -> {"output"};
	$output_file -> openFile(">");
	# Header impression
	Files::printHeader($output_file, $option_hash, $data_hash);
	foreach my $breakpoint (@breakpoints) {
		if (!($breakpoint->isFilteredOut($option_hash, $data_hash))) {
			$breakpoint->printFields($output_file, $option_hash, $data_hash);
		}
	}
}

1;
