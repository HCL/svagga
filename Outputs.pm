package Outputs;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>
use strict;
use warnings FATAL => "all";

use Switch;

# UTILISE
sub determineOutputs {
	my ($option_hash, $data_hash) = @_;
	# Creation of the Impression objects
	my $entities;
	# For the moment, we can impress events OR breakpoints, by sample OR following file raw order
	# For the moment, there is only one type of 
	# TODO : make it mor flexible
	foreach my $entities (@{ $$option_hash{"impression"}{"targetted_entities"} }) {
		if($$option_hash{"impression"}{"impression_logic"} eq "by_sample") {
			SAMPLE: foreach my $sample_name (@{ $$data_hash{"sorted_samples"}{batch} }) {
				my $sample = Samples::findOrDie($sample_name, $data_hash);
				# We do not impress files for reference/non targeted samples
				unless ($sample -> {target}) {
					next SAMPLE;
				}
				my $cluster = "by_sample";
				my $suffixe = $$option_hash{"impression"}{"suffixes"} ? $$option_hash{"impression"}{"suffixes"} : "annotated_".$entities;
				my $output_file_name = $$option_hash{"impression"}{"repertory"}."/".$sample->{name}.".".$suffixe.".tab";
				# We test if we have to follow the impression
				my $valid = testExistence($output_file_name, $option_hash);
				if (!($valid)) {
					next SAMPLE;
				}
				my $file_instance = new Files($output_file_name, "tabulated");
				my $impression = new Impression($file_instance, $entities, $cluster);
				$impression -> {"input_key"} = $sample -> {name};
				push @{ $$data_hash{"impressions"}{"annotatedBreakpoints"} }, $impression;
			}
		} else {
			my $i = 0;
			FILE: foreach my $file (keys %{ $$data_hash{"files"}{$entities} }) {
				# File instance - will be useful to know if the file has to be printed
				my $file_instance = $$data_hash{objects}{files}{$file};
				if ($file_instance -> {not_to_print}) {
					next FILE;
				}
				# Output file name
				my $suffixe = $$option_hash{"impression"}{"suffixes"} ? $$option_hash{"impression"}{"suffixes"} : "annotated_".$entities;
				my $output_file_name = $$option_hash{"impression"}{"repertory"}."/".$file_instance->baseNameWithoutExtension().".".$suffixe.".tab";
				my $valid = testExistence($output_file_name, $option_hash);
				if (!($valid)) {
					next FILE;
				}
				my $output_file_instance = new Files($output_file_name, "tabulated");
				my $cluster = "by_file";
				my $impression = new Impression($output_file_instance, $entities, $cluster);
				$impression -> {"input_key"} = $file;
				push @{ $$data_hash{"impressions"}{"annotatedBreakpoints"} }, $impression;
			}
		}
		# Creation of the folders
		if (!( -d $$option_hash{"impression"}{"repertory"} )){
			Folders::makeRepertoryOrDie($$option_hash{"impression"}{"repertory"});
		}
	}
}

# UTILISE
# Function which tests if an output file exists, and reacts according to options
sub testExistence {
	my ($file_name, $option_hash) = @_;
	my $valid;
	if (-f $file_name) {
		switch ($$option_hash{impression}{existent_files}) {
			case "overwrite" {
				Files::suppressFileOrDie($file_name);
				$valid = 1;
			}
			case "error" {
				die Errors::ExistentOuputFile($file_name);
			}
			case "ignore" {
				$valid = 0;
			}
		}
	} else {
		$valid = 1;
	}
	return($valid);
}

1;
