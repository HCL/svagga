package BreakPoints;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

use Switch;
use List::Util qw(min max sum);

our @ISA = ("Intervals");
# OBJECTS

# BreakPoint Object, which is an interval which can be associated with other intervals, forming events and can belong to a sample
sub new {
	my ($class, $chromosome, $position_5p, $position_3p, $sample, $mate, $parent, $orientation) = @_;
	$class = ref($class) || $class;
	my $breakpoint = $class->Intervals::new( $chromosome, $position_5p, $position_3p );
	# Mate breakpoint
	$breakpoint->{"mate"} = $mate;
	# Sample
	$breakpoint->{"sample"} = $sample ? $sample : "Unknown";
	# Parent call/event
	$breakpoint->{"parent"} = $parent;
	# Orientation
	$breakpoint->{"orientation"} = $orientation;
	# Other attributes, non declarable during instanciation
	$breakpoint->{"genomicContext"} = undef;
	$breakpoint->{"linkedInSamples"} = undef;
	$breakpoint->{"annotations"} = {};
	bless($breakpoint, $class);
	return($breakpoint);
}

sub createBreakpointCoupleFromInfoLine {
	my ($info, $format, $option_hash) = @_;
	if ($format eq "VCF") {
		return createBreakpointCoupleFromParsedVCFLine($info, $option_hash);
	} elsif ($format eq "TAB") {
		return createBreakpointCoupleFromParsedTABLine($info, $option_hash);
	} else {
		die Errors::ProgrammerBug("Non tolerated format for creating breakpoints from calls.\n");
	}
}

sub annotateWithVCFInfo {
	my ($breakpoint, $vcf_elements) = @_;
	# All the INFO fields are stored
	%{ $breakpoint->{"annotations"} } = %{ $$vcf_elements{info} };
	# Also are stored the other VCF information
	$breakpoint->{"annotations"}{"ID"} = $$vcf_elements{id};
	$breakpoint->{"annotations"}{"ALT"} = $$vcf_elements{alt};
	$breakpoint->{"annotations"}{"REF"} = $$vcf_elements{ref};
	$breakpoint->{"annotations"}{"QUAL"} = $$vcf_elements{qual};
	$breakpoint->{"annotations"}{"FILTER"} = $$vcf_elements{filter};
	$breakpoint->{"annotations"}{"#CHROM"} = $$vcf_elements{chrom};
	$breakpoint->{"annotations"}{"POS"} = $$vcf_elements{pos};
}


sub createBreakpointCoupleFromParsedVCFLine {
	my ($vcf_elements, $option_hash) = @_;
	my $chromosome = $$vcf_elements{chrom};
	my $start = $$vcf_elements{pos};
	my $end = $$vcf_elements{pos};
	my ($chromosome_2, $start_2, $end_2, $orientation_2);

	# The CI around POS can extend the first breakpoint
	if($$option_hash{"vcf_parsing"}{"bnd_extension"}) {
		if (defined($$vcf_elements{info}{ $$option_hash{"vcf_parsing"}{"bnd_extension_start"} })) {
			my @CIPOS = split(",", $$vcf_elements{info}{ $$option_hash{"vcf_parsing"}{"bnd_extension_start"} });
			$start -= abs $CIPOS[0]; # In specifications the value msut be negative, but we prefer to prevent errors
			if (!(defined($CIPOS[1]))) {
				$CIPOS[1] = $CIPOS[0];
			}
			$end += abs $CIPOS[1];
		}
	}
	Genomes::dieIfNonexistentChromosome($chromosome);
	my $breakpoint_1 = BreakPoints->new($chromosome, $start, $end);
	$breakpoint_1->annotateWithVCFInfo($vcf_elements);

	# Second breakpoint treatment
	# For translocations, the second breakpoint is described in ALT
	if ($$vcf_elements{info}{SVTYPE} eq "BND") {
		if ($$vcf_elements{alt} =~ m/([^\[\]]*)([\[\]])(\w+)\:(\d+)([\[\]])([^\[\]]*)/) {
			# If the boundary is on the model x[[ or x]], the considered breakpoint (breakpoint_1) is on the direct strand (->)
			if ($1) {
				$breakpoint_1->{"orientation"} = "+";
			# If the boundary is on the model [[x or ]]x, the considered breakpoint (breakpoint_1) is on the reverse strand (<-)
			} elsif ($6) {
				$breakpoint_1->{"orientation"} = "-";
			} else {
				return($breakpoint_1,0);
			}
			# If the boundary is on the model x[[ or [[x, the mate breakpoint (breakpoint_2) is on the direct strand (->)
			if ($2.$5 eq "[[") {
				$orientation_2 = "+";
			# If the boundary is on the model x]] or ]]x, the mate breakpoint (breakpoint_2) is on the reverse strand (<-)
			} elsif ($2.$5 eq "]]") {
				$orientation_2 = "-";
			# Else, there is probably an error in the format
			} else {
				return($breakpoint_1,0);
			}
			$chromosome_2 = $3;
			$start_2 = $4;
			$end_2 = $start_2;
		} else {
			# In case of single breakends, there is no mate
			return($breakpoint_1,0);
		}
	# Else, the END field is used (DUP, DEL, etc.)
	} elsif (defined($$vcf_elements{info}{END})) {
		$chromosome_2 = $breakpoint_1->{"chromosome"};
		$start_2 = $$vcf_elements{info}{END};
		$end_2 = $$vcf_elements{info}{END};
	} else {
		return($breakpoint_1,0);
	}
	# Whatever the way the second breakpoint is discovered, it can have a CIEND
	if($$option_hash{"vcf_parsing"}{"bnd_extension"}) {
		if (defined($$vcf_elements{info}{ $$option_hash{"vcf_parsing"}{"bnd_extension_end"} })) {
			my @CIEND = split(",", $$vcf_elements{info}{ $$option_hash{"vcf_parsing"}{"bnd_extension_end"} });
			$start_2 -= abs $CIEND[0];
			if (!(defined($CIEND[1]))) {
				$CIEND[1] = $CIEND[0];
				die();
			}
			$end_2 += abs $CIEND[1];
		}
	}
	Genomes::dieIfNonexistentChromosome($chromosome_2);
	my $breakpoint_2 = BreakPoints->new($chromosome_2, $start_2, $end_2);
	$breakpoint_2->{"orientation"} = $orientation_2;
	# $breakpoint_2->{annotations}{ID} = $$vcf_elements{info}{MATEID}; # Useless

	return($breakpoint_1,$breakpoint_2);
}

# UTILISE
sub createBreakpointCoupleFromParsedTABLine {
	my ($tab_elements, $option_hash) = @_;

	# We cross all the fields to get the values
	my ($chromosome, $position, $start, $end, $orientation);
	my ($mate_chromosome, $mate_position, $mate_start, $mate_end, $mate_orientation);
	my %annotations;

	foreach my $element (keys %$tab_elements) {
		my $value = $$tab_elements{$element};
		switch ($element) {
			case "Chromosome" {
				$chromosome = $value;
			}
			case "Mate_Chromosome" {
				$mate_chromosome = $value;
			}
			case "Position" {
				$position = $value;
			}
			case "Mate_Position" {
				$mate_position = $value;
			}
			case "Start" {
				$start = $value;
			}
			case "Mate_Start" {
				$mate_start = $value;
			}
			case "End" {
				$end = $value;
			}
			case "Mate_End" {
				$mate_end = $value;
			}
			case "Orientation" {
				$orientation = $value;
			}
			case "Mate_Orientation" {
				$mate_orientation = $value;
			} else {
				$annotations{ $element } = $value;
			}
			
		}
	}

	# BreakPoint Creation
	Genomes::dieIfNonexistentChromosome($chromosome);
	Genomes::dieIfNonexistentChromosome($mate_chromosome);
	my $breakpoint_1 = createBreakpointFromChrPosStartEnd($chromosome, $position, $start, $end);
	%{ $breakpoint_1->{annotations} } = %annotations;
	$breakpoint_1 -> {orientation} = testOrientation($orientation);
	my $breakpoint_2 = createBreakpointFromChrPosStartEnd($mate_chromosome, $mate_position, $mate_start, $mate_end);
	$breakpoint_2 -> {orientation} = testOrientation($mate_orientation);
	return ($breakpoint_1, $breakpoint_2);
}

sub testOrientation {
	my ($orientation) = @_;
	if ($orientation) {
		if ($orientation ne "+" && $orientation ne "-") {
			die Errors::incorrectOrientationFormat($orientation);
		}
	}
	return $orientation;
}

# UTILISE
sub createBreakpointFromChrPosStartEnd {
	my ($chromosome, $position, $start, $end) = @_;
	# Start > Position, but without Start, Position is taking into account
	if ($position) {
		if (!($start)) {
			$start = $position;
		}
	} elsif (!($start)) {
		die Errors::ProgrammerBug("Impossible to create a breakpoint without start or position.\n");
	}
	# Without End, End becomes Start
	if (!($end)) {
		$end = $start;
	}
	return BreakPoints->new($chromosome, $start, $end);
}

# UTILISE
sub linkedTo {
	my ($breakpoint_1, $breakpoint_2) = @_;
	$breakpoint_1->{mate} = $breakpoint_2;
	$breakpoint_2->{mate} = $breakpoint_1;
}

# UTILISE
sub storeBreakpointInHash {
	my ($breakpoint, $call, $hash) = @_;
	# if the breakpoint has an ID, we store it by this one
	if ($breakpoint->{annotations}{ID}) {
		$$hash{$breakpoint->{annotations}{ID}} = $breakpoint;
	} else {
		$$hash{ $call->toSimpleFlatStringWithOrientation() } = $breakpoint;
	}
}

# UTILISE
sub foundBreakPointCoupleInHash {
	my ($target_breakpoint, $mate_breakpoint, $call, $hash) = @_;
	# Variable
	my $found;
	# If the MATEID of the target breakpoint is already stored in the hash
	if ($target_breakpoint->{annotations}{MATEID}) {
		if (defined($$hash{ $target_breakpoint->{annotations}{MATEID} })) {
			# The couple has already been seen
			$mate_breakpoint = $$hash{ $target_breakpoint->{annotations}{MATEID} };
			$found = 1;
		}
	} elsif ($$hash{ $call->toSimpleFlatStringWithOrientation() }) {
		# If the call is already stored in the hash
		$mate_breakpoint = $$hash{ $call->toSimpleFlatStringWithOrientation() };
		$found = 1;
	}
	# If the breakpoint has been found,
	if ($found) {
		$target_breakpoint = $mate_breakpoint->{"mate"};
	}
	return($target_breakpoint, $mate_breakpoint, $found);
}

# UTILISE
sub getBreakPointsFromFiles {
	print "## Acquisition of the Breakpoints ##\n";
	my ($option_hash, $data_hash) = @_;
	foreach my $bp_file (@{ $$option_hash{files}{breakpoint_files} }){
		print "# File : $bp_file\n";
		BreakPoints::getBreakPointsFromAFile($bp_file,$option_hash, $data_hash); # The breakpoint are first stored
	}
	print "\n";
}



# UTILISE
# Function which takes a file of breakpoints and sort them into a hash
sub getBreakPointsFromAFile {
	my ($breakpoint_file, $option_hash, $data_hash) = @_;
	my $is_bed_file;
	# TODO : a function to treat each breakpoint file
	if ($breakpoint_file =~ m/\.bed$/) {
		$is_bed_file = 1;
	}
	open(my $breakpoint_fh, "<", $breakpoint_file) or die(Errors::formatFileOpeningError("breakpoint"));

	# Extraction of the header information
	my $header = <$breakpoint_fh>;
	my $field_hash = Files::determinateHeaderFields($header, $breakpoint_file, 1);

	# Storage of the breakpoints
	foreach my $line (<$breakpoint_fh>) {
		my @elements = Files::megaSplit($line);
		# Because of the header, we can determine the breakpoint location on the line, and other information about it
		my $breakpoint_instance = createBreakpointInstanceFromALine($line, $field_hash, $breakpoint_file);
		Genomes::dieIfNonexistentChromosome($breakpoint_instance->{"chromosome"});
		# By default, we keep the input order for by-file impression
		push @{ $$data_hash{"files"}{"breakpoints"}{$breakpoint_file} }, $breakpoint_instance;
		# The breakpoint instance is also stored by sample and by chromosome to increase speed for the following computations
		$breakpoint_instance->storeAndSortOnChromosomeArray(\@{ $$data_hash{"samples"}{  $breakpoint_instance->{"sample"} }{"breakpoints"}{"chromosomes"}{ $breakpoint_instance->{"chromosome"} } })
	}
	close($breakpoint_fh);
}

# UTILISE 
sub createBreakpointInstanceFromALine {
	my ($line, $header_hash, $breakpoint_file) = @_;
	$line = Files::megaChomp($line);
	my @raw_information = Files::megaSplit($line);
	# Breakpoint elements
	my ($chromosome, $start, $end);
	# From Breakpoint field
	if(defined($$header_hash{"Breakpoint"}) && defined($raw_information[ $$header_hash{"Breakpoint"} ])) {
		if (!(respectsBreakpointNomenclature($raw_information[ $$header_hash{"Breakpoint"} ]))) { # Test of the breakpoint format
			my $error_message = "The following breakpoint is at an incorrect format : ".$raw_information[ $$header_hash{"Breakpoint"} ]."\n";
			die ($error_message);
		}
		($chromosome, $start, $end) = explodeCheckedBreakpoint($raw_information[ $$header_hash{"Breakpoint"} ]);
	}
	# From Chromosome
	if(defined($$header_hash{"Chromosome"}) && defined($raw_information[ $$header_hash{"Chromosome"} ])) {
		if (defined($chromosome) && ($chromosome ne $raw_information[ $$header_hash{"Chromosome"} ])) {
			die "Conflict in Chromosome information, in file :\n$breakpoint_file.\n";
		}
		$chromosome = $raw_information[ $$header_hash{"Chromosome"} ];
	} 
	if (!($chromosome)) {
		die("Cannot determine breakpoint position without Chromosome field, in file :\n$breakpoint_file.\n");
	}
	# From Position
	if(defined($$header_hash{"Position"}) && defined($raw_information[ $$header_hash{"Position"} ])) {
		if (defined($start) && ($start != $raw_information[ $$header_hash{"Position"} ])) {
			die "Conflict in Start information, in file :\n$breakpoint_file.\n";
		}
		$start = $raw_information[ $$header_hash{"Position"} ];
	}
	# From Start
	if(defined($$header_hash{"Start"}) && defined($raw_information[ $$header_hash{"Start"} ])) {
		if (defined($start) && ($start != $raw_information[ $$header_hash{"Start"} ])) {
			die "Conflict in Start information, in file :\n$breakpoint_file.\n";
		}
		$start = $raw_information[ $$header_hash{"Start"} ];
	}
	# From End
	if(defined($$header_hash{"End"}) && defined($raw_information[ $$header_hash{"End"} ])) {
		if (defined($end) && ($end != $raw_information[ $$header_hash{"End"} ])) {
			die "Conflict in End information, in file :\n$breakpoint_file.\n";
		}
		$end = $raw_information[ $$header_hash{"End"} ];
	}
	# Final inspection
	if (!(defined($chromosome)) || !(defined($start))) {
		die ("Start or Chromosome information missing in file :\n$breakpoint_file.\n");
	}
	if (!(defined($end))) {
		$end = $start;
	}
	# Breakpoint instanciation
	my $breakpoint_instance = BreakPoints -> new($chromosome, $start, $end);
	return($breakpoint_instance);
}

# UTILISE
sub explodeCheckedBreakpoint {
	my ($breakpoint) = @_;

	my $chromosome;
	my $start; # Boundaries
	my $end;

	$breakpoint =~ m/^([^\:]+):(.+)$/; # Chromosome
	$chromosome = $1;
	my $boundaries = $2;

	if ($boundaries =~ m/([0-9]+)\-([0-9]+)/) { # Two boundary breakpoint
		$start = $1;
		$end = $2;
	} else {
		$start = $boundaries;  # One boundary breakpoint
	}
	return($chromosome, $start, $end);
}

# UTILISE
# Functions which return 1 if the breakpoint has a correct format
sub respectsBreakpointNomenclature {
	my $breakpoint = $_[0];
	if ($breakpoint !~ /\:/) {
		return(0);
	}
	my @breakpoint_elements = split(":", $breakpoint);
	# Chromosome
	if (!($Genomes::authorized_chromosomes{$breakpoint_elements[0]})) {
		return(0);
	}
	# Boundaries
	if ($breakpoint_elements[1] =~ m/^([0-9]+)\-([0-9]+)$/) { # Two boundary breakpoint
		if ($1 <= $2) {
			return(1);
		} else {
			return(0);
		}
	} elsif ($breakpoint_elements[1] =~ m/^[0-9]+$/) { # One boundary breakpoint
		return(1);
	} else {
		return(0);
	}
}

##### SORT ###########


# OBSOLETE ?
# Function which sorts the breakpoint in order to increase computation speed
sub sortBreakPointsByChromosome {
	my ($data_hash) = @_;
	foreach my $sample (keys %{ $$data_hash{"samples"} }) {
		foreach my $chromosome (@Genomes::ordered_chromosomes) {
			@{ $$data_hash{"samples"}{$sample}{"sorted_breakpoints"}{"chromosomes"}{$chromosome} } = Intervals::sortIntervalArray(@{ $$data_hash{"samples"}{$sample}{"breakpoints"}{"chromosomes"}{$chromosome} });
		}
	}
}

# UTILISE
sub determineSample {
	my ($breakpoint) = @_;
	my $sample;
	# TODO : the options can ask to increase occurrences even if the sample has been already seen
	if (defined($breakpoint -> {parent})) {
		$sample = $breakpoint -> {parent} -> {sample};
	} else {
		$sample = $breakpoint -> {sample};
	}
	return($sample);
}

# UTILISE 
sub determineOccurrence {
	my ($breakpoint) = @_;
	my $occurrences;
	# TODO : the options can ask to increase occurrences even if the sample has been already seen
	if (defined($breakpoint -> {occurrences})) {
		return $breakpoint -> {occurrences};
	} elsif (defined($breakpoint -> {parent} )) {
		return $breakpoint -> {parent} -> {occurrences};
	} else {
		die(Errors::ProgrammerBug("Breakpoint".$breakpoint->toSimpleBPString." has no occurrences, and neither its parent.\n"));
	}
	
}

#UTILISE 
sub updateReciprocalOccurrences {
	my ($breakpoint, $compared_breakpoint, $option_hash) = @_;
	$breakpoint->updateOccurrences($compared_breakpoint, $option_hash);
	$compared_breakpoint->updateOccurrences($breakpoint, $option_hash);
}

# UTILISE
sub updateOccurrences {
	my ($breakpoint, $compared_breakpoint, $option_hash) = @_;
	my $sample = $compared_breakpoint -> determineSample();
	# If no breakpoint are linked to this breakpoint in the compared sample, the occurrences increase
	if (!(defined($breakpoint->{samples}{$sample}{mate}[0]))) {
		$breakpoint->{occurrences}++;
	}
	# We store the breakpoint
	push @{ $breakpoint->{samples}{$sample}{mate} }, $compared_breakpoint;
}

sub isAfter {
	my $target_breakpoint = $_[0];
	my $compared_breakpoint = $_[1];

	# Be careful: to gain some time, the chromosome is not tested # TODO : think about this ?
	# The targeted breakpoint is after the compared one...	
	# If its first position is after..
	if ($target_breakpoint->{"start"} > $compared_breakpoint->{"start"}) {
		return(1);
	# Or, if they are identical, and..
	} elsif ($target_breakpoint->{"start"} == $compared_breakpoint->{"start"}) {
		# Its second boundary is after
		if ($target_breakpoint->{"start"} > $compared_breakpoint->{"start"}) {
			return(1);
		# Or, if they are identical, and..
		} elsif ($target_breakpoint->{"start"} == $compared_breakpoint->{"start"}) {
			# It has a partner which is after the compared_breakpoint partner
			if (!(defined($target_breakpoint->{"mate"})) || !(defined($compared_breakpoint->{"mate"}))) {
				return(0);
			}
			if ($Genomes::authorized_chromosomes{ $target_breakpoint->{"mate"}->{"chromosome"} }{"order"} > $Genomes::authorized_chromosomes{ $compared_breakpoint->{"mate"}->{"chromosome"} }{"order"}) {
				return(1);
			} elsif ($Genomes::authorized_chromosomes{ $target_breakpoint->{"mate"}->{"chromosome"} }{"order"} == $Genomes::authorized_chromosomes{ $compared_breakpoint->{"mate"}->{"chromosome"} }{"order"}) {
				if ($target_breakpoint->{"mate"}->{"start"} > $compared_breakpoint->{"mate"}->{"start"}) {
					return(1);
				} elsif ($target_breakpoint->{"mate"}->{"start"} == $compared_breakpoint->{"mate"}->{"start"}) {
					if ($target_breakpoint->{"mate"}->{"start"} > $compared_breakpoint->{"mate"}->{"start"}) {
						return(1);
					}
				}
			}
		}
	}
	return(0);
}

##### INTERVALS ######

# Function which takes a breakpoint intance, creates a second one with same parameter and return it
sub duplicateBreakPoint {
	my ($breakpoint_instance) = @_;
	my $duplicated_breakpoint_instance = BreakPoints->new($breakpoint_instance->{"chromosome"}, $breakpoint_instance->{"start"}, $breakpoint_instance->{"end"}, $breakpoint_instance->{"sample"}, undef, undef, $breakpoint_instance->{"orientation"});
	return($duplicated_breakpoint_instance);
}


# Function which will interact wuith the option information to return an array of elements (that will be printed)
# TODO: to make it ineract with the option hash
sub get_boundary_information {
	my ($breakpoint, $options) = @_;
	return($breakpoint->{"chromosome"}, $breakpoint->meanBoundary(), $breakpoint->{"mate"}->{"chromosome"}, $breakpoint->{"mate"}->meanBoundary(), $breakpoint->{"BELONGING_INSTANCE"}->{"EVENT"});
}

# Function which returns the mean position of the breakpoint
sub meanBoundary {
	my ($breakpoint_instance) = @_;
	return(ceil(($breakpoint_instance->{"start"} + $breakpoint_instance->{"start"})/2));
}

sub get_summarized_breakpoint {
	my ($breakpoint_instance) = @_;
	return($breakpoint_instance->{"chromosome"}.":".$breakpoint_instance->meanBoundary());
}

# UTILISE
sub toSimpleBPString {
	my ($breakpoint_instance) = @_;
	return $breakpoint_instance->{"chromosome"}.":".$breakpoint_instance->{"start"}."-".$breakpoint_instance->{"end"};
}

# UTILISE
sub toSimpleBPStringWithOrientation {
	my ($breakpoint_instance) = @_;
	my $string = $breakpoint_instance->{"chromosome"}.":".$breakpoint_instance->{"start"}."-".$breakpoint_instance->{"end"};
	if ($breakpoint_instance->{orientation}) {
		$string .= "_(".$breakpoint_instance->{orientation}.")";
	}
	return $string;
}

# Development : useful to print all the breakpoints of the metaSV
sub printBoundaries {
	my ($breakpoint_instance) = @_;
	print $breakpoint_instance->{"start"}."\t".$breakpoint_instance->{"start"}."\n";
}


# UTILISE
# Function which takes two (for the moment) breakpoints and sorts them into an array
sub sortPartnerBreakpoints {
	my @breakpoint_array = @_;
	# The breakpoint can be sorted by chromosome
	if($Genomes::authorized_chromosomes{ $breakpoint_array[0]->{"chromosome"} }{"order"} < $Genomes::authorized_chromosomes{ $breakpoint_array[1]->{"chromosome"} }{"order"}) {
		return(@breakpoint_array);
	} elsif($breakpoint_array[0]->{"chromosome"} eq $breakpoint_array[1]->{"chromosome"}) {
		if($breakpoint_array[0]->{"start"} < $breakpoint_array[1]->{"start"}) {
			return(@breakpoint_array);
		} elsif($breakpoint_array[0]->{"start"} == $breakpoint_array[1]->{"start"}) {
			if($breakpoint_array[0]->{"end"} <= $breakpoint_array[1]->{"end"}) {
				return(@breakpoint_array);
			} else {
				return($breakpoint_array[1], $breakpoint_array[0]);
			}
		} else {
			return($breakpoint_array[1], $breakpoint_array[0]);
		}
	} else {
		return($breakpoint_array[1], $breakpoint_array[0]);
	}
}

# UTILISE
sub mergeWithBreakpoint {
	my ($breakpoint, $added_breakpoint) = @_;
	$breakpoint->extendBreakpointBoundaries($added_breakpoint);
	$breakpoint->updateOrientation($added_breakpoint);
}



# UTILISE
# Function which takes two Breakpoints and enlarge the first with the second one's
sub extendBreakpointBoundaries {
	my ($breakpoint, $added_breakpoint) = @_;
	if ($breakpoint->{"chromosome"} ne $added_breakpoint->{"chromosome"}) {
		die(Errors::ProgrammerBug(Errors::DiscordantChromosomes)); # This bug, if it exists, comes from the program itself and not from user
	}
	$breakpoint->{"start"} = min($breakpoint->{"start"}, $added_breakpoint->{"start"});
	$breakpoint->{"end"} = max($breakpoint->{"end"}, $added_breakpoint->{"end"});
}

# UTILISE
sub updateOrientation {
	my ($breakpoint, $added_breakpoint) = @_;
	my $current_orientation = $breakpoint->{orientation};
	my $compared_orientation = $added_breakpoint->{orientation};
	# If there is already a declared orientation, it is modified only if the new one is different
	if($current_orientation) {
		if ($compared_orientation && $current_orientation ne $compared_orientation) {
			$breakpoint->{orientation} = "both";
		}
	} else {
		# If there is not, it takes the new one orientation without testing if it exists
		$breakpoint->{orientation} = $added_breakpoint->{orientation};
	}
}

# UTILISE
# Function which prints all the breakpoints contained in an array
sub printBreakPointArray {
	my ($array) = @_;
	foreach my $breakpoint (@$array) {
		print $breakpoint->toSimpleBPString();
	}
}





sub annotateBreakPointsWithBedInformation {
	my $breakpoint_hash = $_[0];
	my $beds = $_[1];
	my $chromosome_hash = $_[2];

	foreach my $file (keys  %{ $$breakpoint_hash{"files"} }) {
		foreach my $breakpoint (@{ $$breakpoint_hash{"files"}{$file} }) {
			findClosestBEDPosition($breakpoint, $beds)
		}
	}
}

sub annotateWithGene {
	my ($breakpoint, $gene) = @_;
	my $distance = $breakpoint->distanceTo($gene);
	my $exonic;
	if ($distance == 0) {
		while (my ($key, $transcript) = each %{ $gene->{"transcripts"} }) {
			while (my ($k, $exon) = each %{ $transcript->{"exons"} }) {
				if ($breakpoint->distanceTo($exon) == 0) {
					$exonic = 1;
				}
			}
		}
	}
	$breakpoint->updateTrackAnnotation("Gene", $gene, $distance);
	if ($exonic) {
		$breakpoint->{"annotations"}{"Gene"}{"crossing_exons"}{$gene} = $gene;
	}	
	if ($gene->{"is_omim"}) {
		$breakpoint->updateTrackAnnotation("OMIM", $gene, $distance);
	}	
}

sub findClosestGenesOnChromosome {
	my $breakpoint_instance = $_[0];
	my $chromosome_hash = $_[1];
	my $genes = $_[2];
	my $options = $_[3];

	if (!(defined($$chromosome_hash{$breakpoint_instance->{"chromosome"}}))) { # If there is no annotation on the considered chromosome, we return 1 to exit the sub function
		return(1);
	}
	my $num = 0;
	
	foreach my $gene (keys %{ $$chromosome_hash{ $breakpoint_instance->{"chromosome"} }{"genes"} }) {
		my $distance = distanceBetweenBEDRegions($breakpoint_instance->{"start"}, $breakpoint_instance->{"start"}, $$genes{$gene}->{"start"}, $$genes{$gene}->{"start"});
		if (!(defined($breakpoint_instance->{"annotations"}{"affected_genes"}{"distance"})) || $distance < $breakpoint_instance->{"annotations"}{"affected_genes"}{"distance"}) {
			unless (defined($$genes{$gene}->{"dubious"})) {
				# TODO : work on
				#if ($distance < $$options{}
				$breakpoint_instance->{"annotations"}{"affected_genes"}{"distance"} = $distance;
				$breakpoint_instance->{"annotations"}{"affected_genes"}{"gene"} = $gene;
			}
		}
	}
}


sub findClosestBEDPosition {
	my $breakpoint_instance = $_[0];
	my $beds = $_[1];

	if (!(defined($$beds{"Chromosomes"}{$breakpoint_instance->{"chromosome"}}))) { # If there is no annotation on the considered chromosome, we return 1 to exit the sub function
		return(1);
	}

	my $num = 0;

	BED: foreach my $bed_file (keys %{ $$beds{"Chromosomes"}{$breakpoint_instance->{"chromosome"}}{"BED"} }) {
		my $previous_distance;
		for my $position_number (0 .. $#{ $$beds{"Chromosomes"}{$breakpoint_instance->{"chromosome"}}{"BED"}{$bed_file} }) {
			# For the moment, the considered information is on colomn 4 (3 for perl)
			my $category = $$beds{"Chromosomes"}{$breakpoint_instance->{"chromosome"}}{"BED"}{$bed_file}[$position_number][3]; # Commodity # TODO : the colomn 3 will not be forever the (lonely) colomn of interest
			my $distance = distanceBetweenBEDRegions($breakpoint_instance->{"start"} - 1, $breakpoint_instance->{"start"} - 1, $$beds{"Chromosomes"}{$breakpoint_instance->{"chromosome"}}{"BED"}{$bed_file}[$position_number][1], $$beds{"Chromosomes"}{$breakpoint_instance->{"chromosome"}}{"BED"}{$bed_file}[$position_number][2]);
			# For the moment, the distance is updated for each category of element
			if (!(defined($breakpoint_instance->{"annotations"}{"BED"}{$bed_file}{"ELEMENT_CATEGORY"}{$category}{"DISTANCE"})) || $distance < $breakpoint_instance->{"annotations"}{"BED"}{$bed_file}{"ELEMENT_CATEGORY"}{$category}{"DISTANCE"}) {

				$breakpoint_instance->{"annotations"}{"BED"}{$bed_file}{"ELEMENT_CATEGORY"}{$category}{"DISTANCE"} = $distance;
				# TODO : if interested only by the closest element, we can quite this loop earlier
			}
			# The BED being sorted, if the distance increases, we can quit
			if (defined($previous_distance) && $distance > $previous_distance) {
				#next;
			}
			if ($distance == 0) {
				next BED;
			}
			$previous_distance = $distance;
		}
	}
}

#UTILISE
sub setReciprocalOverlap {
	my ($breakpoint, $compared_breakpoint) = @_;
	# The breakpoints overlap
	$breakpoint->{overlapWith}{$compared_breakpoint} = 1;
	$compared_breakpoint->{overlapWith}{$breakpoint} = 1;
	# So, their parent are linked by breakpoints (used for the annotation of the complex events)
	$breakpoint->{parent}->{linkedTo}{ $compared_breakpoint->{parent} } =  $compared_breakpoint->{parent};
	$compared_breakpoint->{parent}->{linkedTo}{ $breakpoint->{parent} } =  $breakpoint->{parent};
}

#UTILISE
sub overlapWithDistance {
	my ($breakpoint, $compared_breakpoint, $tolerated_distance) = @_;
	if (abs($breakpoint->distanceTo($compared_breakpoint)) <= $tolerated_distance) {
		return(1);
	} else {
		return(0);
	}
}

# UTILISE
# UTILISE 
# Function which...
# We only test if their parent 
sub parentsOverlap {
	my ($breakpoint, $compared_breakpoint) = @_;
	if (defined($breakpoint -> {parent}) && defined($compared_breakpoint -> {parent})) {
		if ($breakpoint -> {parent}->overlap($compared_breakpoint -> {parent})) {
			return(1);
		} else {
			return(0);
		}
	} else {
		die(Errors::ProgrammerBug("The breakpoints should have a parent.\n"));
	}
}


# Function which returns yes if two breakpoint are overlapping according to options (distance)
sub areOverlappingBreakpoints {
	my $breakpoint_1 = $_[0];
	my $breakpoint_2 = $_[1];
	my $options = $_[2];

	if (!(defined($breakpoint_1->{"chromosome"}))) {
		#print $breakpoint_1."\n";
		die("");
	} else {
		#print $breakpoint_1."\n";
	}

	if($breakpoint_1->{"chromosome"} eq $breakpoint_2->{"chromosome"}) {
 		if($$options{"breakpoints"}{"distance"} >= Intervals::distanceBetweenBEDRegions($breakpoint_1->{"start"}, $breakpoint_1->{"start"}, $breakpoint_2->{"start"}, $breakpoint_2->{"start"})) {
			return(1);
		}
	} else {
		return(0);
	}
}





# Function which prints the different fields of the BreakPoint
sub asFormattedBreakpoint {
	my ($breakpoint) = @_;
	my $breakpoint_with_nomenclature = $breakpoint->{"chromosome"}.":". $breakpoint->{"start"};
	if (defined($breakpoint->{"end"})) {
		$breakpoint_with_nomenclature .= "-". $breakpoint->{"end"};
	}
	return($breakpoint_with_nomenclature);
}

sub pli {
	my $gene = $_[0];
	if (defined($gene->{"annotations"}{"ExAC_pLI.txt"})) {
		if($gene->{"annotations"}{"ExAC_pLI.txt"}[0] > 0.9) {
			return $gene->{"name"}." (pLI)";
		}
	}
	return $gene->{"name"};
}

sub getInfo {
	my ($breakpoint, $printedField) = @_;
	my $info = "";
	# Gestion of the "generic" fields " TODO : find a better word
	switch($printedField->{interpreted_track}) {
		case ("Samples") {
			return BasicFunctions::getIfDefined($breakpoint->{"sample"}, "NA");
		}
		case ("Breakpoints") {
			return $breakpoint->asFormattedBreakpoint();
		}
	}
	# Gestion of the fields from BED-like files
	# print $printedField->{interpreted_track}."\t".$printedField->{"target"}."\n";
	my $target;
	my $distance;
	if ($printedField->{"target"} eq "overlapping") {
		$target = "overlapping";
	} elsif ($printedField->{"target"} eq "closest") {
		($target, $distance) = $breakpoint->determineClosest($printedField->{interpreted_track});
	} else {
		die();
	}
	if (defined($breakpoint->{"annotations"}{ $printedField->{interpreted_track} }{$target})) {
		if ($printedField->{"info"} eq "field") {
			return BED::aggregateHashInfo(";", $breakpoint->{"annotations"}{ $printedField->{interpreted_track} }{ $printedField->{"target"} });
		} elsif ($printedField->{"info"} eq "yes_no") {
			return "1";
		} elsif ($printedField->{"info"} eq "distance") {
			if (defined($distance)) {
				return $distance;
			} else {
				die();
			}
		}
	} else {
		return "NA";
	}
}

# UTILISE
sub getBreakpointGenomicContext {
	print "## Determination of the Breakpoint genomic context ##\n";
	my ($option_hash, $data_hash) = @_;
	my $distance_5p = $$option_hash{"raw_options"}{"breakpoint_context_length"};
	my $distance_3p = $$option_hash{"raw_options"}{"breakpoint_context_length"};
	my $genome = $$option_hash{"raw_options"}{"reference_contigs"};
	foreach my $breakpoint_file (keys %{ $$data_hash{"breakpoints"}{"files"} }) {
		foreach my $breakpoint (@{ $$data_hash{"breakpoints"}{"files"}{$breakpoint_file} }) {
			$breakpoint->getGenomicContext($genome, $distance_5p, $distance_3p);
		}
	}
	print "\n";
}
# UTILISE
sub getGenomicContext {
	my ($breakpoint, $genome, $distance_5p, $distance_3p) = @_;
	$breakpoint->{"genomicContext"} = Genomes::prepareGenomicContextGetting($breakpoint->{"chromosome"},
	$breakpoint->{"start"}, $breakpoint-> {"end"}, $genome, $distance_5p, $distance_3p);
}

sub determineClosest {
	my ($breakpoint, $field) = @_;
	my ($distance, $closest) = (undef, "none");
	if (defined($breakpoint->{"annotations"}{$field}{"overlapping"})) {
		($distance, $closest) = (0, "overlapping");
	} else {
		if (defined($breakpoint->{"annotations"}{$field}{"distance_before"})) {
			$distance = $breakpoint->{"annotations"}{$field}{"distance_before"};
			$closest = "before";
		}
		if (defined($breakpoint->{"annotations"}{$field}{"distance_after"})) {
			if (!(defined($distance)) || $breakpoint->{"annotations"}{$field}{"distance_after"} < $breakpoint->{"annotations"}{$field}{"distance_before"}) {
				$distance = $breakpoint->{"annotations"}{$field}{"distance_after"};
				$closest = "after";
			}
		}
		if (!(defined($distance))) {
			$distance = "NA"; # We do not let it undefined to know that the computation has already be done
		}
	}
	return($closest, $distance);
}


# UTILISE
sub isFilteredOut {
	my ($breakpoint, $option_hash, $data_hash) = @_;
	# If only the first breakpoints are asked, the second-in-order breakpoints are not printed
	if ($$option_hash{raw_options}{print_only_first_breakpoint}) {
		if($breakpoint->{order} > 0) {
			return(1);
		}
	}
	# Occurrence filter
	if(defined($$option_hash{events}{filters}{maximum_occurrences}) && defined($breakpoint->{parent}->{occurrences}) && $breakpoint->{parent}->{occurrences} > $$option_hash{events}{filters}{maximum_occurrences}) {
		return(1);
	}
	return(0);
}

# UTILISE
sub printFields {
	my ($source_breakpoint, $output_file, $option_hash, $data_hash) = @_;
	my $tabulatedString = "";

	foreach my $printedField (@{ $$data_hash{"sorted_fields"} }) {
		# Determination of field to be printed
		my $field = $printedField->{interpreted_track};
		my $value;
		my $considered_breakpoint;
		# We test if we have to print the information for the mate breakpoint
		if ( $printedField->{mate}) {
			# We test whether such a mate exists
			if ($source_breakpoint -> {mate}) {
				$considered_breakpoint = $source_breakpoint->{mate};
			} else {
				$value = "Undefined";
			}
		} else {
			$considered_breakpoint = $source_breakpoint;
		}
		# If value is already set, go further
		if (!(defined($value))) {
			# Genes and annotations
			if ($field eq "Gene" || $printedField->{"source_type"}{"annotationFile"}) {
				$value = $considered_breakpoint->dealWithAnnotationFile($printedField, $option_hash);
			# Pre-understood fields (breakpoints, sample, etc.)
			 } elsif ($printedField->{"source_type"}{"basicTrack"}) {
				$value = $considered_breakpoint->dealWithKnownField($field, $option_hash);
			# Other fields
			} elsif ($printedField->{"source_type"}{"callerField"}) {
				$value = $considered_breakpoint->dealWithCallerField($printedField, $option_hash);
			} else {
				die (Errors::ProgrammerBug("This track seems to originate from nowhere and should not pass previous controls : $field.\n"));
			}
			# If always undefined, set it
			if (!(defined($value))) {
				$value = "Undefined";
			}
		}
		$tabulatedString .= $value."\t";
	}
	$tabulatedString =~ s/\t$/\n/;
	print { $output_file -> {"file_handler"} } $tabulatedString;
}

sub dealWithKnownField {
	my ($breakpoint, $field, $option_hash) = @_;
	switch ($field) {
		case "Sample" {
			return $breakpoint->determineSample();
		}
		# Breakpoint
		case "Breakpoint" {
			return $breakpoint->toSimpleBPString();
		}
		case "Breakpoint_Instance" { # Programmer
			return $breakpoint;
		}
		case "Chromosome" {
			return $breakpoint->{chromosome};
		}
		# TODO : return the mean (or other) between start and end (before, we will have to re-sort breakpoints !)
		case "Position" {
			return $breakpoint->{start};
		}
		case "Start" {
			return $breakpoint->{start};
		}
		case "End" {
			return $breakpoint->{end};
		}
		case "Length" {
			 return $breakpoint->{end} - $breakpoint->{start} + 1;
		}
		case "Occurrence" {
			my $value = $breakpoint->{occurrences};
			if (!(defined($value))) {
				$value = 0;
			}
			return($value);
		}
		case "Orientation" {
			return $breakpoint->{orientation};
		}
		case "Order_In_Call" {
			return $breakpoint->{order};
		}
		# Call
		case "Call_Length" {
			 return $breakpoint->{parent}->computeLength();
		}
		case "Call_Occurrence" {
			my $value = $breakpoint->{parent}{occurrences};
			if (!$value) {
				$value = 0;
			}
			return $value;
		}
		case "Link_Occurrence" {
			my $value = $breakpoint->{parent}->{link_occurrences};
			if (!(defined($value))) {
				$value = 0;
			}
			return($value);
		}
		case "Event" {
			if (defined($breakpoint->{parent})) {
				my $parent = $breakpoint->{parent};
				my $event_type = $parent->{event_type};
				if ($event_type eq "Several") {
					if ($$option_hash{raw_options}{show_all_ambiguous_events} && defined($parent -> {all_event_types})) {
						my @events = sort keys %{ $parent -> {all_event_types} };
						$event_type = join(";", @events);
					}
				}
				return $event_type;
			}
		}
		# Caller
		case "Caller" {
			if (defined($breakpoint->{parent})) {
				my $parent = $breakpoint ->{parent};
				my $callers;
				# In function of the previous tasks, the calls will be the event calls
				if ($parent -> isa("Events")) {
					$parent->computeCallerAnnotation();
					$callers = join(";", sort(keys %{ $parent->{callers}{all_callers} }));
				} else {
					# Or the call itself
					$callers = $parent -> {caller};
				}
				return $callers;
			}
		}
		case "Caller_Occurrence" {
			if (defined($breakpoint->{parent})) {
				my $parent = $breakpoint ->{parent};
				my $callers;
				# In function of the previous tasks, the calls will be the event calls
				if ($parent -> isa("Events")) {
					$parent->computeCallerAnnotation();
					$callers = $parent->{callers}{occurrences};
				} else {
					# Or the call itself
					$callers = 1;
				}
				return $callers;
			}
		}
		# Other
		case "Complex_Event_ID" {
			return $breakpoint->{parent}->getComplexEvent($option_hash);
		} else {
			die Errors::ProgrammerBug("Unknown basic field for breakpoint annotation : $field.\n");
		}
	}
}

sub dealWithAnnotationFile {
	my ($breakpoint, $printedField, $option_hash) = @_;
	my $field = $printedField->{interpreted_track};
	my $value;
	# Identify the to-be-printed target(s)
	my @targets;
	if ($printedField->{"target"} eq "overlapping") {
		if (defined($breakpoint->{"annotations"}{$field}{"overlapping"})) {
			TARGET: foreach my $key (keys %{  $breakpoint->{"annotations"}{$field}{"overlapping"} }) {
				my $target = $breakpoint->{"annotations"}{$field}{"overlapping"}{$key};
				if ($field eq "Gene") {
					if (defined($printedField->{focus}) && $printedField->{focus} eq "omim") {
						if (!($target ->{"omim"})) {
							next TARGET;
						}
					}
				}
				push @targets, $target;
			}
		}
	} else {
		die();
	}

	# Obtention of the target information to be printed
	if (defined($targets[0])) {
		if ($printedField->{"info"} eq "content") {
			my @content;
			# For each target, the information is store in info, unless it is a gene field
			foreach my $target (@targets) {
				if ($field eq "Gene") {
					push @content, $target->{"name"};
				} else {
					push @content, $target->{"info"};
				}
			}
			if (!(defined($content[0]))) {
				die "Annotation not found. Are the following elements censed to have an \"info\" key ? \n$field\t".join("\t", keys %{ $breakpoint->{"annotations"}{$field}{"overlapping"} })."\n";
			} else {
				$value = join(";",@content);
			}
		}elsif ($printedField->{"info"} eq "yes_no") {
			$value = "1";
		} else {
			die();
		}
	}
	# If value is not set, set it with the corresponding "false" value
	unless($value) {
		if ($printedField->{"info"} eq "content") {
			$value = "NA";
		} elsif ($printedField->{"info"} eq "yes_no") {
			$value .= "0";
		} else {
			die($printedField->{"info"}."\n");
		}
	}

}

sub dealWithCallerField {
	my ($breakpoint, $printedField, $option_hash) = @_;
	my $field = $printedField->{interpreted_track};
	my @values;
	my $string = "";
	# If the breakpoint is already annotated we return the value
	if ($breakpoint->{annotations}{$field}) {
		return $breakpoint->{annotations}{$field};
	} else {
	# Else, we compute it
		return $breakpoint->computeAnnotation($printedField);
	}
}

sub computeAnnotation {
	my ($breakpoint, $printedField) = @_;
	# We get all the children calls of the parent (theoritically, an Events)
	my @parent_call_or_parent_call_children = $breakpoint->{parent}->getCallChildren();
	my @values;
	foreach my $call (@parent_call_or_parent_call_children ) {
		# For each call, we get the annotation of the breakpoints with the same order than the targetted breakpoint
		my $annotations = $call->{breakpoints}[ $breakpoint->{order} ]->{annotations};
		my $value = %$annotations{ $printedField->{interpreted_track} };
		if ($value) {
			push @values, $value;
		} else {
			push @values, "NA";
		}
	}
	$breakpoint->{annotations}{ $printedField->{interpreted_track} } = join(";", @values);
	return $breakpoint->{annotations}{ $printedField->{interpreted_track} }
		# die join("\t", sort keys %{  }."\n";
			# if ($call->{annotations}{$field}) {
			# 	my $value = $call->{annotations}{$field};
			# 	if ($printedField->{compute} eq "none") {
			# 		die();
			# 		$string .= $call->{caller}.":".$value.";";
			# 	} elsif ($printedField->{compute} eq "mean") {
			# 		die();
			# 		if (looks_like_number($value)) {
			# 			push @values, $value;
			# 		} else {						
			# 			if ($$option_hash{computation}{value_control} eq "strict") {
			# 				die(Errors::abnormalValue($value, "number"))
			# 			} elsif ($$option_hash{computation}{value_control} eq "omit") {
			# 				# TODO : write in a log file ?
			# 			} elsif ($$option_hash{computation}{value_control} eq "signal") {
			# 				return "Abnormal value";
			# 			}
			# 		}
			# 	}
			# }
	# if ($printedField->{compute} eq "none") {
	# 	$string =~ s/\;$//;
	# 	return($string);
	# } elsif ($printedField->{compute} eq "mean" && $values[0]) {
	# 	return(Values::round(sum(@values)/@values, $$option_hash{computation}{round}));
	# }
}

# UTILISE
sub meanPosition {
	my ($breakpoint) = @_;
	if (!(defined($breakpoint->{meanPosition}))) {
		$breakpoint->{meanPosition} = Values::round(($breakpoint->{start} + $breakpoint->{end})/2, 0);
	}
	return $breakpoint->{meanPosition};
}
1;
