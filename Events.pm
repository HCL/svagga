package Events;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';
no warnings 'recursion';

use Switch;

# An Event is a Link which can contain several Calls
our @ISA = ("Links");

# Hash table which contains all the tolerated events
our %known_events =(
	"Deletion" => {
		"Involved_Chromosomes" => "Same",
		"Copy_Number" => 1,
	},
	"Duplication" => {
		"Involved_Chromosomes" => "Same",
		"Copy_Number" => 1,
	},
	"Inversion" => {
		"Involved_Chromosomes" => "Same",
		"Copy_Number" => 0,
	},
	"Translocation" => {
		"Involved_Chromosomes" => "Different",
		"Copy_Number" => 0,
	},
	"Insertion" => {
		"Involved_Chromosomes" => "Both",
		"Copy_Number" => 0,
	},
	"Breakend" => {
		"Involved_Chromosomes" => "Both",
		"Copy_Number" => 0,
	},
	"Unknown" => {
		"Involved_Chromosomes" => "Both",
		"Copy_Number" => 0,
	},
	"Ambiguous" => {
		"Involved_Chromosomes" => "Both",
		"Copy_Number" => 0,
	},
	"Several" => {
		"Involved_Chromosomes" => "Both",
		"Copy_Number" => 0,
	},
);

# Event (= by-sample call cluster object), which contains a list of "mixable" calls
sub new {
	my ($class, $calls) = @_;
	$class = ref($class) || $class;
	my $event = {};
	bless($event, $class);
	foreach my $call (@$calls) {
		# Event instances
		if (!( $event->{"calls"}{$call} )) {
			$event->addCallToEvent($call);
		}
	}
	return($event);
}


# UTILISE
sub interpretedEventFromInfoLine {
	my ($info, $format, $option_hash) = @_;
	if ($format eq "VCF") {
		return interpretedEvent($$info{info}{SVTYPE}, $option_hash);
	} else {
		return interpretedEvent($$info{Event}, $option_hash);
	}
}


# UTILISE
sub getTargetEventFiles {
	my ($option_hash, $data_hash) = @_;
	if (defined(@{ $$option_hash{files}{target_event_files} }[0])) {
		print "## Acquisition of the target Events ##\n";
		foreach my $target_event_file (@{ $$option_hash{files}{target_event_files} }) {
		 	print "- File : ".$target_event_file."\n";
		 	Calls::getCallsFromAFile($target_event_file, "target_events", $option_hash, $data_hash);
		}
	}
	print "\n";
}

sub getEventReferenceOccurrences {
	my ($option_hash, $data_hash) = @_;
	print "## Computation of the event occurrences in reference samples ##\n";
	SAMPLE: foreach my $sample_nb (0 .. $#{ $$data_hash{"sorted_samples"}{batch} }) {
		my $sample =  ${ $$data_hash{"sorted_samples"}{batch} }[$sample_nb];
		foreach my $reference_sample (@{ $$data_hash{"sorted_samples"}{reference} }) {
			# If both samples are not targetted, compare each other has not interest
			if ($sample -> {target}) {
				print " - ".$sample." versus ".$reference_sample."\n";
				compareTwoSampleBPEvents($sample, $reference_sample, $option_hash, $data_hash);
				# TODO : CNV based events (not at all the same strategy)
				# TODO : CNV against BP
			}
		}
	}
	print "\n";
}

# UTILISE
sub addCallToEvent {
	my ($event, $call) = @_;
	$call->{"parent"} = $event;
	$event->{"calls"}{$call} = $call;
	$event->updateEventInformation($call);
}

sub updateEventInformation {
	my ($event, $call, $option_hash) = @_;
	# Breakpoints
	$event->updateBreakpointInformation($call);
	# Event type
	$event->updateEventTypeInformation($call, $option_hash);
	$event->checkOrAttributesInformation($call->{sample}, "sample");
	# print $event->toSimpleEventStringWithCalls()."\n";
}

# UTILISE
# Function which checks that an information is conformed in an event
sub checkOrAttributesInformation {
	my ($event, $value, $field) = @_;
	if (defined($event->{$field})) {
		if ($value ne $event->{$field}) {
			die Errors::ProgrammerBug($event->toSimpleEventStringWithCalls()."The Event field $field has an incorrect value :\n".$value." versus ".$event->{$field}."\n");
		}
	} else {
		$event->{$field} = $value;
	}
}

# UTILISE
sub overlap {
	my ($event, $compared_event) = @_;
	# If at least one pair does not cluster, the calls are not clustered
	foreach my $breakpoint_nb (0 .. $#{ $event->{breakpoints} }) {
		if (!($event->{breakpoints}[$breakpoint_nb]->{overlapWith}{ $compared_event->{breakpoints}[$breakpoint_nb] })) {
			return(0);
		}
	}
	return(1);
}

# # UTILISE
# sub computeLength {
# 	my ($event) = @_;
# 	if (!(defined($event->{length}))) {
# 		if  ( $event->{breakpoints}[0]->{chromosome} ne $event->{breakpoints}[1]->{chromosome} ) {
# 			$event->{length} = "NA";
# 		} else {
# 			$event->{length} = $event->{breakpoints}[1]->meanPosition() - $event->{breakpoints}[0]->meanPosition();
# 		}
# 	}
# 	return $event->{length};
# }

# UTILISE
sub updateBreakpointInformation {
	my ($event, $call) = @_;
	# Add the call to the existent ones
	if (defined(${ $event->{breakpoints} }[0])) {
		foreach my $breakpoint_nb (0 .. $#{ $event->{breakpoints} }) {
			$event->{breakpoints}[$breakpoint_nb]->mergeWithBreakpoint($call->{breakpoints}[$breakpoint_nb]); # Each breakpoint of the Cluster is updated according to the corresponding breakpoint of the SV
		}
	} else {
		# Or a duplicate the call as an event
		$event->copyCallInformation($call);
	}
}

sub copyCallInformation {
	my ($event, $call) = @_;
	foreach my $breakpoint_nb (0 .. $#{ $call->{breakpoints} }) {
		$event->{breakpoints}[$breakpoint_nb] = $call->{breakpoints}[$breakpoint_nb]->duplicateBreakPoint();
		$event->{breakpoints}[$breakpoint_nb]->{parent} = $event;
	}
	$event->{breakpoints}[0]->{mate} = $event->{breakpoints}[1];
	$event->{breakpoints}[1]->{mate} = $event->{breakpoints}[0];
	$event->{breakpoints}[0]->{order} = 0;
	$event->{breakpoints}[1]->{order} = 1;
}

# UTILISE 
# Function which tests the concordance of the Event and a call # TODO : make it more clever/modular according to options
sub updateEventTypeInformation {
	my ($event, $call, $option_hash) = @_;
	$event->{event_type} = determineFusionEventType($event->{event_type}, $call->{event_type});
	# if(defined($event->{event_type})) {
		# For the moment, if such a case occurs, this is an error
		# if ($event->{event_type} ne $call->{event_type}) {
			# We store the different types of observed events for the event
			# $event->{all_event_types}{ $call -> {event_type} } = 1;
			# $event->{all_event_types}{ $event -> {event_type} } = 1;
			# delete $event->{all_event_types}{"Several"};
			# We set the event type to Several - this terms will not be printed,
			# but allows to not allocate the "Ambiguous" key, excepted if a call is really called "Ambiguous"
			# (tu vas probablement oublier ce que cela signifiait, mais fais-toi confiance !)
			# $event->{event_type} = "Several";
			# die(Errors::ProgrammerBug("The Event and the Call are not the same kind of event.\n"));
		# }
	# }
	# else {
	# 	if (!(defined($call->{event_type}))) {
	# 		die(Errors::ProgrammerBug("No event defined for this call...\n"));
	# 	}
	# 	$event->{event_type} = $call->{event_type};
	# }
}

sub determineFusionEventType {
	my ($event_1, $event_2, $option_hash) = @_;
	if ($event_1) {
		if ($event_2) {
			if ($event_1 eq $event_2) {
				return $event_1;
			} elsif ($event_1 eq "Several" || $event_2 eq "Several") {
			# Several versus Breakend/Deletion/Anything gives Several
				return "Several";
			} elsif ($event_1 eq "Breakend" || $event_2 eq "Breakend") {
				# Deletion/Duplication versus Breakend gives Deletion/Duplication
				if ($event_1 eq "Breakend") {
					return $event_2;
				} else {
					return $event_1;
				}
			} else {
				# Deletion versuis Duplication gives Several
				return "Several";
			}
		} else {
			return $event_1;
		}
	} else {
		return $event_2;
	}
	die();
}

# UTILISE
# Function which returns all the call of an event
sub getCallChildren {
	my ($event) = @_;
	my @calls;
	foreach my $call (keys %{ $event->{calls} }) {
		push @calls, $event->{calls}{$call};
	}
	return(@calls);
}

# UTILISE
sub toSimpleEventString {
	my ($event) = @_;
	my $string = $event->{event_type}."\n";	
	foreach my $breakpoint (@{ $event->{breakpoints} }) {
		$string .= "breakpoint ".$breakpoint->{order}.":".$breakpoint->{start}."-".$breakpoint->{end}."\n";
	}
	return($string);
}

# UTILISE
sub toSimpleEventStringWithCalls {
	my ($event) = @_;
	my $string = "Event $event:\n".$event->toSimpleEventString;
	$string .= "Calls :\n";
	foreach my $call (keys %{ $event -> {calls} }) {
		$string .= "## ".$event -> {calls}{$call}->toSimpleCallString;
	}
	return($string);
}


# UTILISE
sub toSimpleEventStringWithCallsWithOrientation {
	my ($event) = @_;
	my $string = "Event $event:\n".$event->toSimpleEventString;
	$string .= "Calls :\n";
	foreach my $call (keys %{ $event -> {calls} }) {
		$string .= "## ".$event -> {calls}{$call}->toSimpleFlatStringWithOrientation()."\n";
	}
	return($string);
}

# UTILISE
sub toAnnotatedEventString {
	my ($event) = @_;
	my $string = $event->toSimpleEventStringWithCalls;
	foreach my $key (keys %{ $event }) {
		$string .= "## ".$key." : ".$event -> {$key}."\n";
	}
	return($string);
}

# UTILISE
sub computeCallerAnnotation {
	my ($event) = @_;
	#  The computation is assumed to be done once
	if (!(defined($event -> {callers}))){
		foreach my $call (keys %{ $event -> {calls} }) {
			$call = $event -> {calls}{$call};
			if (!(defined( $event->{callers}{all_callers}{ $call -> {caller} } ))) {
				$event->{callers}{all_callers}{ $call -> {caller} } = 1;
				$event ->{callers}{occurrences}++;
			}
		}
	}
}



# UTILISE
# Recursive function which adds events to a temporary hash until all linked events have already been seen
sub getComplexEvents {
	my ($event, $event_temp_hash) = @_;
	$$event_temp_hash{$event} = $event;
	foreach my $call (keys %{ $event -> {calls} }) {
		foreach my $compared_call (keys %{ $event -> {calls}{$call}->{linkedTo} }) {
			my $compared_event = $event -> {calls}{$call}->{linkedTo}{$compared_call}->{parent};
			if (defined($compared_event) && !($$event_temp_hash{$compared_event})) {
				$compared_event->getComplexEvents($event_temp_hash);
			}
		}
	}
}

# Function which interprets the Events
sub interpretedEvent {
	my ($tested_event, $option_hash) = @_;

	# If the event has not to be interpreted, it is returned as it came
	if (!($$option_hash{"events"}{"interpretation"})) {
		return $tested_event;
	}

	# Return an interpreted event
	my $returned_event;
	switch (lc($tested_event)) {
		case "" {
			$returned_event = "Breakend";
		}
		case /^tand/ {
			$returned_event = "Tandem repeat";
		}
		case /^del/ {
			$returned_event = "Deletion";
		}
		case /^dup/ {
			$returned_event = "Duplication";
		}
		case /^trans/ {
			$returned_event = "Translocation";
		}
		case /^ins/ {
			$returned_event = "Insertion";
		}
		case /^inv/ {
			$returned_event = "Inversion";
		}
		case /^ambig/ {
			$returned_event = "Several";
		}
		case /^bnd/ {
			$returned_event = "Breakend";
		}
		case /^undeter/ {
			$returned_event = "Breakend";
		}
		case /^unknown/ {
			$returned_event = "Breakend";
		}
		case /^sev/ {
			$returned_event = "Several";
		}
		else {
			print "Unknown event : $tested_event.\n";
			$returned_event = "Breakend";
		}
	}
	return($returned_event);
}


# Function which tests if the involved chromosomes and the event are consistent. If not, returns "ambiguous"
sub testEventTypeConsistency {
	my $tested_event = $_[0];
	my $chr1 = $_[1];
	my $chr2 = $_[2];

	my $new_event;
	my $message;
	if(!(defined($known_events{$tested_event}))) {
		# $new_event = "Breakend";
		# $message = "unknown event.\n";
	}elsif ($known_events{$tested_event}{"Involved_Chromosomes"} eq "Same" && $chr1 ne $chr2) {
		$new_event = "Breakend";
		$message = "incompatible with the presence of two different chromosomes ($chr1 and $chr2)";
	} elsif ($known_events{$tested_event}{"Involved_Chromosomes"} eq "Different" && $chr1 eq $chr2) {
		$new_event = "Breakend";
		$message = "should be associated with two different chromosomes, and not one ($chr1)";
	}
	if ($new_event) {
		print "##Incoherence for the event $tested_event : $message. ";
		print "Will be considered as a $new_event.\n";
	}
	return($tested_event);
}

