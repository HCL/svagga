###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

# In order to charge the modules present in the Svagga folder
use Cwd 'abs_path';
use File::Basename;
use lib dirname( abs_path $0 );
use Getopt::Long; Getopt::Long::Configure ("bundling");

# Svagga and Svagga associated librairy loading
use Librairies;

##  VARIABLES
our %options;
our %data;

## PARAMETERS AND WORK CONFIGURATION
GetOptions (\%{ $options{"raw_options"} }, @Arguments::options_and_arguments) or die("#### An error occurred in options and arguments.\n".$!);
Version::version(\%options);
Options::parametrization(\%options, $ARGV[0], \%data);

## INPUT ACQUISITION
# Reference files
if ($options{"subtasks"}{"configureReference"}) {
	Genomes::configureReference(\%options, \%data);
}
# Printed fields
if ($options{"subtasks"}{"getPrintedFields"}) {
	PrintedFields::getPrintedFields(\%options, \%data);
}
# Variant caller files
# if ($options{"subtasks"}{"configureVariantCallers"}) {
# 	VariantCaller::configureVariantCallers(\%options, \%data); # TAF
# }
# Annotation files (BED, etc.)
# if ($options{"subtasks"}{"configureAnnotationFiles"}) {
# 	AnnotationFiles::configureAnnotationFiles(\%options, \%data);
# }
# BreakPoints
if ($options{"subtasks"}{"getBreakPointsFromFiles"}) {
	BreakPoints::getBreakPointsFromFiles(\%options, \%data); # TAF : contrÃ´ler chromosomes
}
# Calls
if ($options{"subtasks"}{"getCallsFromFiles"}) {
	Calls::getCallsFromFiles(\%options, \%data);
}
# Events
if($options{"subtasks"}{"getTargetEventFiles"}) {
	Events::getTargetEventFiles(\%options, \%data);
}

## CONFIGURATION AND VALIDATION FROM INPUTS
Options::configurationAfterInputAcquisition(\%options, \%data);
# Printed fields
if ($options{"subtasks"}{"getPrintedFields"}) {
	PrintedFields::testPrintedFieldConsistency(\%options, \%data);
	Options::subtaskConfigurationAfterFieldDetermination(\%options, \%data);
}

## CALL MANIPULATION
if($options{"subtasks"}{"compareCallWithTargetEvents"}) {
	Calls::compareCallWithTargetEvents(\%options, \%data);
}
if($options{"subtasks"}{"aggregateCallsBySample"}) {
	Calls::aggregateCallsBySample(\%options, \%data);
}

## ANNOTATIONS
# Genes files (GFF)
if ($options{"subtasks"}{"getGenesFromFiles"}) {
	# TAF : si pas de champs imprimés concernés => OSEF des gènes
	Genes::getGenesFromFiles(\%options, \%data); # TAF : contrôler chromosomes
}
# Annotation files
if ($options{"subtasks"}{"getAnnotationsFromFiles"}) {
	AnnotationFiles::getAnnotationsFromFiles(\%options, \%data);
}
# TODO
# Gene annotation files
if ($options{"subtasks"}{"geneAndAnnotationFileCrossing"}) {
	AnnotationFiles::annotateWithGenes(\%options, \%data);
}
# Sample occurrences
if($options{"subtasks"}{"getBreakpointOccurrences"}) {
	Samples::getBreakpointOccurrences(\%options, \%data);
}
if($options{"subtasks"}{"getEventReferenceOccurrences"}) {
	Events::getEventReferenceOccurrences(\%options, \%data);
}
# Breakpoints
if ($options{"subtasks"}{"breakpointAnnotation"}) {
	AnnotationFiles::annotateBreakPoints(\%options, \%data);
}
if ($options{"subtasks"}{"geneAndBreakpointCrossing"}) {
	Genes::annotateBreakPoints(\%options, \%data);
}
if ($options{"subtasks"}{"getBPGenomicContent"}) {
	BreakPoints::getBreakpointGenomicContext(\%options, \%data);
}

## PRINTING
Outputs::determineOutputs(\%options, \%data);
if($options{"subtasks"}{"printAnnotatedBreakPoints"}) {
	Impression::printFiles(\%options, \%data);
}