package Files;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

use Switch;

# Formats and Extensions
my %formats;

my %extensions = (
	"sam" => {
		"format" => "SAM",
	},
	"bam" => {
		"format" => "bam",
	},
	"vcf" => {
		"format" => "VCF",
	},
	"tab" => {
		"format" => "TAB",
	},
	"gff" => {
		"format" => "GFF",
	},
	"gff2" => {
		"format" => "GFF2",
	},
	"gff3" => {
		"format" => "GFF3",
	},
	"gtf" => {
		"format" => "GTF",
	},
);

# UTILISE
# File Object, which contains a file name, a format, a status of opening and a file handler
sub new {
	my ($class, $name, $format) = @_;
	$class = ref($class) || $class;
	my $file = {};
	bless($file, $class);
	# The fields are not tested on the fly because they can be filled after the file instantiation
	$file->{"name"} = $name ? $name : die (Errors::ProgrammerBug("No file name have been given in argument for File Object.\n")); # Full name
	$file->{"base_name"} = nameWithoutAddress($name);
	$file->{"format"} = $format ? $format : determineFormat($name);
	$file->{"status"} = "closed";
	$file->{"file_handler"} = undef;
	$file->{"content_type"} = undef;
	$file->{"precise_content"} = undef;
	$file->{"not_to_print"} = undef;
	# Specific files
	$file->{"sample"} = undef;
	$file->{"caller"} = undef;
	return($file);
}

# UTILISE
sub createAndStoreFile {
	my ($file_name, $data_hash) = @_;
	my $file_instance = Files -> new($file_name);
	$$data_hash{objects}{files}{$file_instance -> {name}} = $file_instance;
	return $file_instance;
}

sub interpreteName {
	my ($file_instance) = @_;
	if ($file_instance->{"base_name"} =~ m/^([^\.]+)\.([^\.]+)\./) {
		$file_instance->{sample} = $1;
		$file_instance->{caller} = $2;
	} else {
		die Errors::fileNameInterpretation($file_instance->{"name"});
	}
}

# UTILISE
sub determineFormat {
	my ($name) = @_;
	my $format = "Unknown";
	if ($name =~ m/\.([^\.]+)$/) {
		my $extension = $1;
		if($extensions{$extension}{format}) {
			$format = $extensions{$extension}{format};
		}
	}
	return $format;
}

# Global functions
sub openFile {
	my ($file, $mode) = @_;
	# If the file is already opened, there is a programmer error
	if ($file->{"status"} eq "opened") {
		die Errors::ProgrammerBug("File ".$file->{"name"}." : you want to open an already opened file")."\n";
	}
	if (!(defined($mode))) {
		$mode = "<"; # No risk
	}
	# Mode determination
	switch($mode) {
		case ("<") {
			$file -> {"mode"} = "reading";
		}
		case (">") {
			$file -> {"mode"} = "writing";
		} else {
			die (Errors::ProgrammerBug("Unknown mode of file opening : $mode"));
		}
	}
	open($file->{"file_handler"}, $mode, $file->{"name"}) or die(Errors::formatFileOpeningError( $file->{"format"} ));
	$file->{"status"} = "opened";
}

# UTILISE
sub testFileOpening {
	my ($file) = @_;
	if ($file->{"status"} eq "closed") {
		$file->openFile();
	}
}

# UTILISE
sub closeFile {
	my $file = $_[0];
	close($file->{"file_handler"});
	$file->{"status"} = "closed";
}


sub extension {
	my $file_name = $_[0];
	$file_name =~ m/\.([^\.]+)$/;
	return($1);
}

sub nameWithoutAddress {
	my $file_name = $_[0];
	$file_name =~ s/\\/\//g;
	$file_name =~ s/.*\/([^\/]*)$/$1/;
	return($file_name);
}

sub baseName {
	my ($file) = @_;
	my $base_name = $file->{name};
	$base_name =~ s/\\/\//g;
	$base_name =~ s/.*\/([^\/]*)$/$1/;
	return($base_name);
}

sub baseNameWithoutExtension {
	my ($file) = @_;
	my $base_name = $file->baseName();
	$base_name =~ s/\.[^\.]+$//;
	return($base_name);
}

# UTILISE
sub treatHeader {
	my ($file) = @_;
	$file->testFileOpening();
	# For the moment, we treat only VCF files
	if($file->{format} eq "VCF") {
		# Get all header lines
		while (my $line = readline($file->{"file_handler"})) {
			# Information is added, but the #CHROM line returns 0, allowing to stop the process
			if (!(VCF::addHeaderInformationFromLine(\%{ $file->{header} }, $line))) {
				last;
			}
		}
	# And tabulated
	} else {
		$file->storeHeaderFields();
	}
}

# UTILISE
sub testReference {
	my ($file) = @_;
	# For the moment, we treat only VCF files
	if($file->{format} eq "VCF") {
		VCF::testFileInstanceReference($file);
	}
}

# UTILISE
# Function which "chomps" the string but taking into account the "\r" characters
sub megaChomp {
	my $line = $_[0];
	$line =~ s/\r//g;
	chomp($line);
	return($line);
}

# Function which split after megaChomping
sub megaSplit {
	my $line = megaChomp($_[0]);
	my $separator = determineSeparator($line);
	my @elements = split($separator, $line);
	return(@elements);
}

# UTILISE
sub storeHeaderFields {
	my ($file) = @_;
	my $header = readline($file->{"file_handler"});
	if ($header) {
		$header = Files::megaChomp($header);
		$file->{header}{raw_content} = $header."\n"; # Can be useful but for the VCF mainly ?
		my $separator = determineSeparator($header);
		my @header_elements = split($separator, $header);
		my %dejavu;
		# We store each field in the file header
		foreach my $field_number (0 .. $#header_elements) {
			my $field_name = $header_elements[$field_number];
			my $field = Fields::createNewField($field_name);
			# Some fields can be seen twice, then we took the already seen one after having eventually normalized the name
			if ($dejavu{ $field->{precise_interpreted_track} }) {
				$field = $dejavu{ $field->{precise_interpreted_track} };
			} else {
				$dejavu{ $field->{precise_interpreted_track} } = $field;
				$field->{position} = $field_number;
			}
			push @{ $file->{header}{raw_fields} }, $field;
			$file->{header}{fields}{ $field->{precise_interpreted_track} } = $field;
		}
	} else {
		die(Errors::noHeaderForFile($file->{name}));
	}
}


# UTILISE
sub printHeader {
	my ($output_file, $option_hash, $data_hash) = @_;
	my $tabulatedString = "";
	foreach my $printedField (@{ $$data_hash{"sorted_fields"} }) {
		$tabulatedString .= $printedField->{"name"}."\t";
	}
	$tabulatedString =~ s/\t$/\n/;
	my $fh = $output_file -> {"file_handler"};
  	print { $output_file -> {"file_handler"} } $tabulatedString;
}

# UTILISE
#TODO : do better
sub determineSeparator {
	my ($header) = @_;
	if ($header =~ m/\t/) {
		return "\t";
	} else {
		return ";";
	}
}

sub testIntervalFileIsSorted {

}



# UTILISE
# Function which determines if the given argument is a file, a list of file
sub getFilesFromArgumentOrList {
	my $file_array = $_[0];
	my @files;
	foreach my $files (@$file_array) {
		my @tmp_files = split(",", $files);
		FILE: foreach my $file (@tmp_files) {
			if (!(-e $file)) {
				die(Errors::inexistentFile($file));
			}
			if ($file =~ m/\.list$/) {
				push @files, getFilesFromList($file);
			} else {
				push @files, $file;
			}
		}
	}
	return(@files);

}

# UTILISE
sub getFilesFromList {
	my $list_file = $_[0];
	my @tmp_array;
	open(my $list_fh, "<", $list_file) or die(Errors::fileOpeningError($list_file));
	foreach my $file (<$list_fh>) {
		$file = megaChomp($file);
		if (!(-e $file)) {
			if (-e Folders::getFolder($list_file)."/".$file) {
				$file = Folders::getFolder($list_file)."/".$file;
			} else {
				die(Errors::inexistentFile($file));
			}
		}
		if (!($file eq "")) {
			push @tmp_array, $file;
		}
	}
	return(@tmp_array);
}

sub extractFileName {
	my $file = $_[0];
	if ($file =~ m/\//) {
		$file =~ s/.+\/([^\/]+)$/$1/;
	}
	return($file);
}

sub get_file_from_getopt {
	my $raw_getopt_files = $_[0];
	my $mandatory_option = $_[1];
	my $file_type = $_[1];

	if ($mandatory_option && !(defined($raw_getopt_files))) {
		die("No $file_type have been given in inputs.\n");
	}
	my @files;

	# File storage
	foreach (@$raw_getopt_files) {
		my @getopt_files = split(",", $_);
		foreach my $getopt_file (@getopt_files) {
			if ($getopt_file =~ m/\.list/) {
				push @files, getFilesFromList($getopt_file);
			} else {
				push @files, $getopt_file;
			}
		}
	}
	# File existence testing
	foreach my $file (@files) {
		if (!(-e $file)) {
			die(Errors::inexistentFile($file));
		}
	}
	return(@files);
}

# UTILISE
sub suppressFileOrDie {
	my ($file) = @_;
    my $command = "rm -f ".$file;
    my $error = system ($command); # System returns "0" if no error occured
    if ($error) {
        die "Impossible to suppress the file :\n".$file."\n";
    }
}

# # OBSOLETE
# # Header functions
# sub determinateHeaderFields {
# 	die(); # A remplacer
# 	# Variables
# 	my ($header, $file , $strict) = @_;

# 	# Information extraction
# 	$header = megaChomp($header);
# 	my $separator = determineSeparator($header);
# 	my @header_elements = split($separator, $header);
# 	my (%fields, @fields);

# 	foreach my $field_nb (0 .. $#header_elements) {
# 		my $field = Fields::createNewField($header_elements[$field_nb]);
# 		# If the field is identified, it is both stored in a hash and in an array
# 		if ($field->{"source_type"}{"basicTrack"}) {
# 			if(defined($fields{$field})) {
# 				# If a field is present several times, it is 
# 				my $i = 2;
# 				while(defined($fields{$field."_".$i})) {
# 					$i++;
# 				}
# 				$field = $field."_".$i;
# 			}
# 			$fields{$field} = $field_nb;
# 			push @fields, $field;
# 		#if not, it could lead to a fatal error
# 		} else {
# 			die();
# 			if($strict){
# 				die("File $file.\nHeader element not understood : ".$header_elements[$field_nb]."\n")
# 			# But for the file majority, it is without effect
# 			} else {
# 				$fields{ $header_elements[$field_nb] } = $field_nb;
# 				push @fields, $header_elements[$field_nb];
# 			}
# 		}
# 	}
# 	return(\%fields);
# }

1;

