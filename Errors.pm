package Errors;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings;

# Pre-concieved messages

sub Warnings {
	return "### WARNINGS ###\n";
}

sub Documentation {
	return "Please refer to the documentation.\n"
}

# Program bugs

sub ProgrammerBug {
	my $error_message = $_[0]; 
	$error_message = "Programmer (not user) bug :\n".$error_message."\n";
	$error_message .= "Please contact us with the precise information about how it occurred.\n";
	return($error_message);
}

# Inconsistency

sub rawOrderImpossibleIfCallAggregation {
	my $error_message = "Impossible to print the raw breakpoints (--impression_logic raw_order) in case of aggregation step (--aggregate_calls).\n";
	return $error_message;
}

# Inputs - File existence

sub inexistentFile {
	my $file = $_[0];
	my $error_message = "Problem with input :  non existent file $file.\n";
}

sub inexistentFormatFile {
	my ($file, $format) = @_;
	my $error_message = "Problem with input :  non existent file $file";
	if (defined($format)) {
		$error_message .= " - format $format.\n";
	} else {
		$error_message .= ".\n";
	}
	return($error_message);
}

sub fileOpeningError {
	my $file = $_[0];
	my $error_message = "Error in input; please furnish an existent and readable $file.\n";
	return($error_message);
}

sub formatFileOpeningError {
	my ($format, $file) = @_;
	my $error_message = "Error in input; please furnish an existent and readable $format file";
	if(defined($file)) {
		$error_message .= ":\nFile $file not openable.\n";
	} else {
		$error_message .= ".\n";
	}
	return($error_message);
}

sub noHeaderForFile {
	my ($file_name) = @_;
	my $error_message = "No header found for file :".$file_name."\n";
	return ($error_message);
}

# Inputs - File name

sub fileNameInterpretation {
	my ($file) = @_;
	my $error_message = "The file name of $file does not allow interpretation of sample and/or caller.\n";
	return($error_message);
}

# Inputs - File formats

sub badlyFormattedFile {
	my ($file, $format) = @_;
	my $error_message = "The file $file seems badly formatted. Please check the specificity of the $format format.\n";
	return($error_message);
}

sub badFormatForFileAmongPossibilities {
	my ($file, $option, $format, $file_array) = @_;
	my $error_message = "The extension of the file $file ($format format) does not correspond to the desired format for $option.\n\nThe possible formats are :\n- ".join("\n- ",@$file_array)."\n";
	return($error_message);
}

sub indispensableField {
	my ($file, $format, $field) = @_;
	my $error_message = "Bad input for file $file ($format) : $field field missing.\n";
	return($error_message);
}

# Inputs - Tabulated specificites

sub callFileInvalidHeader {
	my ($file) = @_;
	my $error_message = "The $file call file does not contain all the required fields :\n- ".join("\n- ", @Calls::indispensableFields)."\n";
	return($error_message);
}

sub missingColumnInLine {
	my ($file, $column_number, $line) = @_;
	my $error_message = "The $file call file does not contain data for position $column_number at the following line :\n$line\n";
	return($error_message);
}

# Inputs - VCF specificities

sub absentID {
	my ($file, $line) = @_;
	my $error_message = "The $file VCF file does not contain ID at the following line :\n$line\n";
	return($error_message);
}

sub absentMATEID {
	my ($file, $line) = @_;
	my $error_message = "The $file VCF file does not contain MATEID at the following line :\n$line\n";
	return($error_message);
}

sub alreadySeenID {
	my ($file, $id, $line) = @_;
	my $error_message = "The MATEID $id has already been seen in the $file VCF file :\n$line\n";
	return($error_message);
}

sub malformedVCFInfoLine {
	my ($line) = @_;
	my $error_message = "Problem in the INFO fields of the line :\n$line\n";
	return($error_message);
}

# Inputs - Reference

sub unknownChromosome {
	my ($chromosome) = @_;
	my $error_message = "The chromosome $chromosome has not been declared in reference.\n";
	return($error_message);
}

sub badLengthChromosome {
	my ($chromosome, $file_length, $reference_length) = @_;
	my $error_message = "The chromosome length of $chromosome ($file_length) is different than this declared in reference ($reference_length).\n";
	return($error_message);
}

sub unknownChromosomeInFile {
	my ($chromosome, $file) = @_;
	my $error_message = UnknownChromosome($chromosome);
	$error_message .= "File : ".$file."\n";
	return($error_message);
}
# Variant callers

# sub unknownCallingMethod {
# 	my $method = $_[0];
# 	my $error_message = "Unknown calling method : $method\n.Please check the variant caller configuration file.\n";
# 	return($error_message);
# }

# sub noInformationForVariantCaller {
# 	my $caller = $_[0];
# 	my $error_message = "Incomplete input(s) : the variant caller \"$caller\" has not been configured.\n";
# 	return($error_message);
# }

# Value
sub abnormalValue {
	my ($value, $expectation) = @_;
	die "Problem with the value \"$value\", supposed to be a $expectation.\nYou should modify the source file, or the control value options.\n";
}

sub incorrectOrientationFormat {
	my ($value) = @_;
	my $error_message = "Incorrect orientation ($value). Only \"-\" and \"+\" are understood values.\n";
	return ($error_message);
}

# Outputs
sub formatFileWritingError {
	my $format = $_[0];
	my $error_message = "Problem encountered when writing $format output ! Please make sure you have the right to create it.\n";
	return($error_message);
}
sub ExistentOuputFile {
	my ($file) = @_;
	my $error_message = "Existent output file : \n$file\nYou may either change output repertory, or switch the --existent_output_files (--eof) option to \"i(gnore)\" or \"o(verwrite)\".\n";
	return($error_message);
}


sub DiscordantChromosomes {
	return("The two breakpoints you want to extend do not belong to the same chromosome !\n");
}

sub DiscordantEvents {
	return("The metaSV and the componing SV are not the same kind of event.\n");
}

sub UndefinedRank {
	return("The BreakPoint instance has no RANK defined.\n");
}

1;
