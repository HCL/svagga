package Folders;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings;

# UTILISE
sub makeRepertoryOrDie {
    my ($folder) = @_;
    my $command = "mkdir ".$folder;
    my $error = system ($command); # System returns "0" if no error occured
    if ($error) {
        die "Impossible to create the folder :\n".$folder."\n";
    }
}

sub getFolder {
    my ($folder) = @_;
    $folder =~ s/\\/\//g; # For Windows-based terminal like my Laragon...
    $folder =~ s/(.*\/)[^\/]*$/$1/;
    return $folder;
}

1;