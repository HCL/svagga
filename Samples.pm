package Samples;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

# Sample Object, which contains a sample name
sub new {
	my ($class, $name) = @_;
	$class = ref($class) || $class;
	my $sample = {};
	bless($sample, $class);

	$sample->{"name"} = $name;

	$sample->{"target"} = 0;
	$sample->{"reference"} = 0;
	$sample->{"batch"} = 0;
    $sample->{"seen"} = 0;
    $sample->{"declared"} = 0;

	return($sample);
}

# UTILISE
sub find {
    my ($sample_name, $data_hash) = @_;
    return $$data_hash{objects}{samples}{$sample_name} ? $$data_hash{objects}{samples}{$sample_name} : 0;
}

sub findOrDie {
    my ($sample_name, $data_hash) = @_;
    my $sample = find($sample_name, $data_hash);
    if (!($sample)) {
        die (Errors::ProgrammerBug("Non existent Sample : $sample_name ."));
    }
    return($sample);
}

# UTILISE
sub findOrNew {
    my ($sample_name, $data_hash) = @_;
    my $sample = find($sample_name, $data_hash);
    if (!($sample)) {
        $sample = Samples->new($sample_name);
        # The sample instance will be trackable by their name
        $$data_hash{objects}{samples}{$sample_name} = $sample;
        # $$data_hash{objects}{samples}{$sample} = $sample;
    }
    return($sample);
}

sub setDeclared {
    my ($sample) = @_;
    $sample->{declared} = 1;
}

# UTILISE
sub setReference {
    my ($sample) = @_;
    if ($sample ->{batch}) {
        die ("The sample ".$sample -> {name}."cannot be used as reference AND as non-reference.\n")
    }
    $sample->{reference} = 1;
}

# UTILISE
sub setBatch {
    my ($sample) = @_;
    if ($sample ->{reference}) {
        die ("The sample ".$sample -> {name}."cannot be used as reference AND as non-reference.\n")
    }
    $sample->{batch} = 1;
}

# UTILISE
sub setTarget {
    my ($sample) = @_;
    if ($sample ->{reference}) {
        die ("The sample ".$sample -> {name}."cannot be used as reference AND be targetted.\n")
    }
    $sample->{target} = 1;
}

# UTILISE
sub setSeen {
    my ($sample) = @_;
    $sample->{seen} = 1;
}

# UTILISE
sub setReciprocalComparison {
    my ($sample, $compared_sample) = @_;
    $sample->{comparedTo}{$compared_sample} = 1;
    $compared_sample->{comparedTo}{$sample} = 1;
}

# UTILISE
sub afterInputControl {
    my ($option_hash, $data_hash) = @_;
    # All samples that have been seen are necessarily in $$data_hash{"samples"}
	my @Samples = sort keys %{ $$data_hash{"objects"}{"samples"} };
    @{ $$data_hash{"sorted_samples"}{all} } = @Samples; # Useless ? ...
   	$$option_hash{samples}{total_seen} = @Samples;
    # Input sample classification
	my @Unknown_Samples;
    foreach my $sample_name (@Samples) {
        my $sample = $$data_hash{"objects"}{"samples"}{$sample_name};
        # Reference samples
        if ( $$data_hash{samples}{$sample_name}{reference_calls} ) {
            $sample->setReference();
            push @{ $$data_hash{"sorted_samples"}{reference} }, $sample_name;
        }

        if ( $$data_hash{samples}{$sample_name}{calls} ) {
            $sample->setBatch();
            push @{ $$data_hash{"sorted_samples"}{batch} }, $sample_name;
        }
        # If targets has been declared
        if ($$option_hash{treated_options}{target_sample}) {
            # If a sample has been declared, but not seen, die
            if ($sample->{declared} && !$sample->{seen}) {
				push @Unknown_Samples, $sample->{name};
            }
        # Else, all samples are targets
        } else {
            if (!($sample->{reference})) {
                $sample->setTarget();
            }
        }
    }
	# If a target sample is not seen in the inputs, emit an error
	if (defined($Unknown_Samples[0])) {
        die "The following reference samples have not been found in input files :\n- ".join("\n- ", @Unknown_Samples)."\n";
	}
}

# UTILISE
sub getBreakpointOccurrences {
	my ($option_hash, $data_hash) = @_;
	print "## Computation of the sample occurrences ##\n";
	SAMPLE: foreach my $sample_name (@{ $$data_hash{"sorted_samples"}{batch} }) {
		my $sample =  $$data_hash{"objects"}{"samples"}{$sample_name};
		foreach my $compared_sample_name (@{ $$data_hash{"sorted_samples"}{batch} }) {
			my $compared_sample = $$data_hash{"objects"}{"samples"}{$compared_sample_name};
			# If both samples are not targetted, compare each other has not interest
			if (($sample -> {target} || $compared_sample -> {target}) && $compared_sample_name ne $sample_name && !($sample->{comparedTo}{$compared_sample})) {
				print " - ".$sample_name." versus ".$compared_sample_name."\n";
				Samples::compareBreakpoints($sample, $compared_sample, $option_hash, $data_hash);
				# TODO : CNV based events (not at all the same strategy)
				# TODO : CNV against BP
			}
            # We store the fact the two samples have been compared
            $sample->setReciprocalComparison($compared_sample);
		}
	}
    print "\n";
}

# UTILISE
sub  compareBreakpoints {
	my ($sample, $compared_sample, $option_hash, $data_hash) = @_;
	my $event_array = $sample->currentArray();
	my $compared_event_array = $compared_sample->currentArray();
    my $sample_name = $sample->{name};
    my $compared_sample_name = $compared_sample->{name};

	CHROMOSOME: foreach my $chromosome (@Genomes::ordered_chromosomes) {
		# We keep in memory the next breakpoint to check for comparison :
		# indeed, if a target breakpoints is too far after a compared breakpoint,
		# a fortiori the next target breakpoint will be too far too
		my $next_compared_breakpoint = 0;
		BP: foreach my $breakpoint (@{ $$data_hash{samples}{$sample_name}{$event_array}{chromosomes}{$chromosome} } ) {
			if (!(defined(${ $$data_hash{samples}{$compared_sample_name}{$compared_event_array}{chromosomes}{$chromosome} }[$next_compared_breakpoint]))) {
				next CHROMOSOME;
			}
			foreach my $compared_breakpoint_nb ($next_compared_breakpoint .. $#{ $$data_hash{samples}{$compared_sample_name}{$compared_event_array}{chromosomes}{$chromosome} } ) {
				my $compared_breakpoint = ${ $$data_hash{samples}{$compared_sample_name}{$compared_event_array}{chromosomes}{$chromosome} }[$compared_breakpoint_nb];
				# print $compared_breakpoint_nb."\n";
				# If the two breakpoints cross, this is stored
				if($breakpoint->overlapWithDistance($compared_breakpoint, $$option_hash{"breakpoints"}{"distance"})) {
					$breakpoint->setReciprocalOverlap($compared_breakpoint);
					$breakpoint->updateReciprocalOccurrences($compared_breakpoint, $option_hash);
					# If they are respectively the last breakpoints of their, events...
					if ($breakpoint->{order} == 1 && $compared_breakpoint->{order} == 1) {
						my ($parent, $compared_parent) = ($breakpoint->{parent}, $compared_breakpoint->{parent});
						# ...and if all the breakpoints of the two events match...
						if ($parent->overlap($compared_parent)) {
							# ... at least the association if the two event types differ
							$parent->updateReciprocalLinkOccurrences($compared_parent, $option_hash);
							# ...and the event are identical if the two event types coincide
							if ($parent->{event_type} eq $compared_parent->{event_type} || $$option_hash{events}{ignore_event_type}){
								$parent->updateReciprocalEventOccurrences($compared_parent, $option_hash);
							}
						}
					}
				} else {
					# If the compared breakpoint is too far before the target,
					# increase the compared_beakpoint for the next targets
					if(($compared_breakpoint->{end} + $$option_hash{"breakpoints"}{"distance"}) < $breakpoint->{"start"}) {
						$next_compared_breakpoint = $compared_breakpoint_nb + 1;
					}
				}
			}
		}
	}
}

# UTILIE
sub currentArray {
    my ($sample) = @_;
    my $array = "calls";
    if ($sample ->{aggregated}) {
        $array = "aggregated_calls";
    }
    if ($sample ->{reference}) {
        $array = "reference_".$array;
    }
    return($array);
}

1;