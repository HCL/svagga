package Options;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

use Switch;
use POSIX;

###### MODULES AND OPTIONS

# All the available modules
our %tasks = (
	# Giving a list of breakpoint, return annotated breakpoints
	# "breakpointAnnotation" => {
	# 	"synonyms" => ["bpa", "bpAnn"],

	# },
	"structuralVariantAnnotation" => {
		"synonyms" => ["sva", "svA", "svAnn", "svAnnot", "svAnnotation"],
	},
	# "breakpointGenomicContext" => {
	# 	"synonyms" => ["bgc", "bct", "bpcontext", "context"],
	# },
	# "variantFileFormatting" => {
	# 	"synonyms" => ["vff"],
	# },
	# "testTask" => {
	# 	"synonyms" => ["ft", "fake_task"],
	# },
);

# All the available options
our %all_options =(

	# Samples
	"target_samples" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			}
		},
	},

	# Input files
	"interpret_input_file_names" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			}
		},
		"yes_or_no" => 1,
	},

	"call_files" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 1,
			},
			"variantFileFormatting" => {
				"mandatory" => 0,
			}
		},
		"file" => 1,
	},

	"reference_call_files" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			}
		},
		"file" => 1,
	},

	"excluded_call_files" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			}
		},
		"file" => 1,
	},

	"target_call_files" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			}
		},
		"file" => 1,
	},

	# Other inputs
	"reference_contigs" => {
		"related_tasks" => {
			# Mandatory because of the needed length of the chromosome
			"breakpointGenomicContext" => {
				"mandatory" => 1,
			},
			"structuralVariantAnnotation" => {
				"mandatory" => 1,
			},
			# Maybe at terms it will be necessary to all tasks ? (cleaner)
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
		"file" => 1,
	},

	# Configuration
	# "variant_caller_configuration_file" => {
	# 	"related_tasks" => {
	# 		"structuralVariantAnnotation" => {
	# 			"mandatory" => 0,
	# 		},
	# 	},
	# 	"file" => 1,
	# },

	# Annotations
	"annotation_file_configuration" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			},
			"breakpointAnnotation" => {
				"mandatory" => 0,
			},
		},
		"file" => 1,
	},

	"gene_files" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
		"file" => 1,
	},

	"omim_files" => {
		"related_tasks" => {
			"breakpointAnnotation" => {
				"mandatory" => 0,
			},
		},
		"file" => 1,
	},

	"gene_annotation_files" => {
		"related_tasks" => {
			"breakpointAnnotation" => {
				"mandatory" => 0,
			},
		},
		"file" => 1,
	},

	# Breakpoint options
	"breakpoint_files" => {
		"related_tasks" => {
			"breakpointAnnotation" => {
				"mandatory" => 1,
			},
			"breakpointGenomicContext" => {
				"mandatory" => 1,
			},
		},
		"file" => 1,
	},

	"maximum_breakpoint_distance" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 1,
			},
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
		"positive" => 1,
	},

	"breakpoint_context_length" => {
		"related_tasks" => {
			"breakpointGenomicContext" => {
				"mandatory" => 1,
			},
		},
	},

	# VCF parsing - Breakpoint extension
	"bnd_extension" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			},
		},
		"yes_or_no" => 1,
	},
	"bnd_extension_start" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			},
		}
	},
	"bnd_extension_end" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			},
		}
	},

	# Call merging
	"ignore_event_type" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
		"yes_or_no" => 1,
	},
	
	"ignore_bp_orientation" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
		"yes_or_no" => 1,
	},

	"force_bnd_aggregation" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
		"yes_or_no" => 1,
	},
	
	"aggregate_calls" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
		"yes_or_no" => 1,
	},

	# Events
	"interprete_events" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
		"yes_or_no" => 1,
	},
	"event_maximum_occurrence" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			},
		},
		"positive" => 1,
	},
	"event_maximum_frequency" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			},
		},
		"frequence" => 1,
	},
	"target_event_files" => {
		"related_tasks" => {
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			},
		},
		"file" => 1,
	},

	# Values
	"round" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
	},

	# Value control
	"value_control" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
	},
	"strict_value_control" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
	},
	"omit_abnormal_values" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
	},
	"signal_abnormal_values" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
	},

	# Outputs
	"output_format" => {
		"related_tasks" => {
			"variantFileFormatting" => {
				"mandatory" => 0,
			},
		},
	},
	"output_directory" => {
		"related_tasks" => {
			"breakpointAnnotation" => {
				"mandatory" => 0,
			},
			"structuralVariantAnnotation" => {
				"mandatory" => 0,
			},
		},
	},
	"output_suffix" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
	},
	"print_only_first_breakpoint" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
	},
	"no_partner_breakpoint_printing" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
	},
	"show_all_ambiguous_events" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
			},
		},
	},
	"printed_field_configuration" => {
		"related_tasks" => {
			"breakpointAnnotation" => {
				"mandatory" => 1,
			},
			"structuralVariantAnnotation" => {
				"mandatory" => 1,
			},
		},
		"file" => 1,
	},
	"existent_output_files" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
				"advised" => 1, # TODO : taking into account the advised options 
			},
		},
	},

	"impression_logic" => {
		"related_tasks" => {
			"every_tasks" => {
				"mandatory" => 0,
				"advised" => 1, # TODO : taking into account the advised options 
			},
		},
	},

);



# Main function which calls parametrization subfuctions
sub parametrization {
	my ($option_hash, $task, $data_hash) = @_;
	print "\n";
	determineTask($option_hash, $task); # Determination of the desired task (TODO: to make available a succession of tasks?)
	firstOptionTreatment($option_hash, $data_hash); # A first treatment of the basic options
	testTaskAndOptions($option_hash); # Test concordance between the selected task and the furnished options
	testOptionCoherence($option_hash); # Test concordance between the selected options
	testFileExistence($option_hash); # Test that all files exist
	decideSubTasks($option_hash); # Decide the subtasks which will be performed based on options
	testSubtasksAndOptions($option_hash); # Test concordance between the ongoing subtasks and the furnished options
	configureAllOptions($option_hash); # Configuration of all the options (arguments or default values)
}


# UTILISE
# Usage message with the available tasks
sub existingTask {
	my $message = "The first argument has to be the desired task. Available tasks:\n";
	my @tasks = sort keys (%tasks);
	foreach my $task (@tasks) {
		$message .= " - $task";
		if (defined($tasks{$task}{"synonyms"}[0])) {
			$message .= " (".$tasks{$task}{"synonyms"}[0].")";
		}
		$message .= "\n";
	}
	return($message."\n\n");
}

# UTILISE
# Function which takes the first argument of the command line, and determine the task the user want to perform
sub determineTask {

	# Variable
	my ($option_hash, $task) = @_;
	my $identified_task;

	# Without any defined task, the program stops
	if (!(defined($task))) {
		die existingTask();
	# Without any known task, the program stops too
	} elsif (!(defined($tasks{$task}))) {
		# But it accepts different shortcuts/synonyms for each task
		foreach my $known_task (keys %tasks) {
			foreach my $synonym_task (@{ $tasks{$known_task}{synonyms} }) {
				if ($synonym_task eq $task) {
					$identified_task = $known_task;
				}
			}
		}
	} else {
		$identified_task = $task;
	}

	# Attribution and printing
	if(defined($identified_task)) {
		print "## Requested task : $identified_task ##\n\n";
		$$option_hash{task} = $identified_task;
	} else {
		# We delete the wrong task from the hash before printing
		delete $tasks{$task};
		die "###### Unknwon task: $task.\n\n".existingTask()."\n\n";
	}
}
# UTILISE
sub firstOptionTreatment {
	# Variables
	my ($option_hash, $data_hash) = @_;
	# Creating a temporary variable is mandatory,
	# if not some options are created in the option hash and disturb for the following tests
	my %temp_options = %{ $$option_hash{"raw_options"} };
	print "## Treatment of the raw options ##\n\n";
	# First global treatment
	foreach my $option (keys %temp_options) {
		# Yes or no options
		if ($all_options{$option}{yes_or_no}) {
			$$option_hash{treated_options}{$option} = yes_or_no($temp_options{$option}, $option);
		}
		# Positive numbers
		if ($all_options{$option}{positive}) {
			if ($temp_options{$option} < 0) {
				die("The option --$option must be positive.\n");
			}
		}
		# Frequencies
		if ($all_options{$option}{frequence}) {
			if ($temp_options{$option} < 0 || $temp_options{$option} > 1) {
				die("The option --$option must be included between 0 and 1.\n");
			}
		}
		# There is a special treatment for file options		
		if ($all_options{$option}{file}) {
			push @{ $$option_hash{"to_be_treated_files"} }, $option;
		}
	}

	# Value control
	if ($$option_hash{raw_options}{value_control}) {
		if ($$option_hash{raw_options}{value_control} eq "strict" || $$option_hash{raw_options}{value_control} eq "omit" || $$option_hash{raw_options}{value_control} eq "signal") {
			$$option_hash{treated_options}{value_control} = $$option_hash{raw_options}{value_control};
		} else {
			die("Unknown mode of value controlling (option --value_control/--vc) : ".$$option_hash{raw_options}{value_control}."\n");
		}
	}

	# Samples
	if ($temp_options{target_samples}) {
		foreach my $sample_names (@{$temp_options{target_samples}}) {
			my @Sample_name = split(",", $sample_names);
			foreach my $sample_name (@Sample_name) {
				$$option_hash{treated_options}{target_sample}{$sample_name} = 1;
				push @{ $$option_hash{treated_options}{target_samples} }, $sample_name;
				# Sample instanciation

				my $sample = Samples::findOrNew($sample_name, $data_hash);
				$sample -> setTarget($data_hash);
				$sample -> setDeclared($data_hash)
			}
		}
	}

	# Call files
	if ($temp_options{call_files}) {
		$$option_hash{treated_options}{are_input_call_files} = 1;
	}

	# Existent output file decision
	if ($temp_options{existent_output_files}) {
		switch ($temp_options{existent_output_files}) {
			case "overwrite" {
				$$option_hash{treated_options}{existent_output_files} = "overwrite";
			}
			case "o" {
				$$option_hash{treated_options}{existent_output_files} = "overwrite";
			}
			case "ignore" {
				$$option_hash{treated_options}{existent_output_files} = "ignore";
			}
			case "i" {
				$$option_hash{treated_options}{existent_output_files} = "ignore";
			}
			case "error" {
				$$option_hash{treated_options}{existent_output_files} = "error";
			}
			case "e" {
				$$option_hash{treated_options}{existent_output_files} = "error";
			} else {
				die("Unknown choice for existent output file decicison (option --existent_output_files/--eof) : ".$temp_options{existent_output_files}."\n");
			}
		}
	}

	# Impression logic
	if ($temp_options{impression_logic}) {
		switch ($temp_options{impression_logic}) {
			case "sample" {
				$$option_hash{treated_options}{impression_logic} = "by_sample";
			}
			case "s" {
				$$option_hash{treated_options}{impression_logic} = "by_sample";
			}
			case "raw" {
				$$option_hash{treated_options}{impression_logic} = "raw_order";
			}
			case "r" {
				$$option_hash{treated_options}{impression_logic} = "raw_order";
			} else {
				die("Unknown choice for impression logic (option --impression_logic/--il) : ".$temp_options{impression_logic}."\n");
			}
		}
	}

	# TODO
	# Some specific options of impression (for example, by file but sorted) can leads to new subtasks of sorting
}

# UTILISE
sub yes_or_no {
	my ($value, $option) = @_;
	switch ($value) {
		case "yes" {
			return 1;
		}
		case "y" {
			return 1;
		}
		case "1" {
			return 1;
		}
		case "no" {
			return 0;
		}
		case "n" {
			return 0;
		}
		case "0" {
			return 0;
		} else {
			die "Not a yes or no argument for the --$option option : $value.\n";
		}
	}
}

# UTILISE
sub testTaskAndOptions {
	# Variables
	my ($option_hash) = @_;
	# Will be used to store the mandatory options that have been forgotten
	my @to_be_treated_files,
	my @forgotten_options;

	print "## Test of the concordance between options and task ##\n\n";

	# First, we test that the options are consistent with the desired task
	foreach my $option (keys %{ $$option_hash{"raw_options"} }) {
		# The options have to belong to the task, or to be general options
		if (!(defined($all_options{$option}{"related_tasks"}{ $$option_hash{task} })) && !(defined($all_options{$option}{"related_tasks"}{"every_tasks"}))) {
			die("## The option \"$option\" is not allowed with the task \"".$$option_hash{task}."\".\n");
		}
	}

	# Second, we test that all the mandatory options of the task have been given
	foreach my $option (keys %all_options) {
		if ($all_options{$option}{"related_tasks"}{ $$option_hash{task} }{mandatory} || $all_options{$option}{"related_tasks"}{"every_tasks"}{mandatory}) {
			if( !(defined( $$option_hash{"raw_options"}{$option}))) {
				push @forgotten_options, $option;
			}
		}
	}
	if (defined($forgotten_options[0])) {
		die "The module ".$$option_hash{task}." require some mandatory options :\n--".join("\n--", @forgotten_options)."\n";
	}

	# Specific task control
	if($$option_hash{"task"} eq "breakpointAnnotation"){
		if (!($$option_hash{raw_options}{annotation_file_configuration}) && !($$option_hash{raw_options}{gene_files})) {
			die("The module breakpointAnnotation needs at least one annotation file (--annotation_file_configuration or --gene_files)\n");
		}
	} 
	if($$option_hash{"task"} eq "structuralVariantAnnotation"){
		if (!(defined($$option_hash{treated_options}{are_input_call_files}))) {
			die("The module structuralVariantAnnotation needs at least one sv file (--call_files or --target_call_files)\n");
		}
	}

	# Outputs and tasks
}

# Function which checks that there is no inconsistency in the selected options
sub testOptionCoherence {
	my $option_hash = $_[0];
	if (defined($$option_hash{files}{gene_annotation_files}) && !(defined($$option_hash{files}{gene_files}))) {
		die("Impossible to take into account gene annotations without any Gene gff file.\n");
	}
}

# Function which checks that all the to be opened files exist
sub testFileExistence {
	my $option_hash = $_[0];
	foreach my $file (@{ $$option_hash{"to_be_treated_files"} } ) { # TODO : not the same treatment for unic file (for example, some configuration files) that for multiple files (for example, VCF files)
		if (ref($$option_hash{raw_options}{$file}) eq 'ARRAY') {
			@{ $$option_hash{files}{$file} } = Files::getFilesFromArgumentOrList(\@{ $$option_hash{raw_options}{$file} });
		} else {
			if (!(-e $$option_hash{raw_options}{$file})) {
				die(Errors::inexistentFile($$option_hash{raw_options}{$file}));
			}
			$$option_hash{files}{$file} =  $$option_hash{raw_options}{$file};
		}
	}
}

# Function which determines all the subtasks that are going to be performed
sub decideSubTasks {
	my ($option_hash) = @_;
	# Input acquisition
	# Reference genome
	if ($$option_hash{files}{"reference_contigs"}) {
		$$option_hash{"subtasks"}{"configureReference"} = 1;
	}
	# Printed fields
	if ($$option_hash{files}{"printed_field_configuration"}) {
		$$option_hash{"subtasks"}{"getPrintedFields"} = 1;
		$$option_hash{"subtasks"}{"getPrintedFields"} = 1;
	}
	# Annotations files
	if ($$option_hash{files}{"annotation_file_configuration"}) {
		$$option_hash{"subtasks"}{"configureAnnotationFiles"} = 1;
		$$option_hash{"subtasks"}{"getAnnotationsFromFiles"} = 1;
		$$option_hash{"subtasks"}{"breakpointAnnotation"} = 1;
	}
	if ($$option_hash{files}{gene_files}) {
		$$option_hash{"subtasks"}{"getGenesFromFiles"} = 1;
		$$option_hash{"subtasks"}{"geneAndBreakpointCrossing"} = 1;
	}
	# Breakpoints
	if ($$option_hash{files}{breakpoint_files}) {
		$$option_hash{"subtasks"}{"getBreakPointsFromFiles"} = 1;
	}
	if ($$option_hash{"task"} eq "breakpointAnnotation") {
		$$option_hash{"subtasks"}{"annotateBreakPoints"} = 1;
	}
	if ($$option_hash{"task"} eq "breakpointGenomicContext") {
		#TODO : when task is bp annotation, genomic context could be asked
		$$option_hash{"subtasks"}{"getBPGenomicContent"} = 1;
	}
	# Calls
	if ($$option_hash{treated_options}{are_input_call_files}) {
		$$option_hash{"subtasks"}{"getCallsFromFiles"} = 1;
	}
	if ($$option_hash{treated_options}{"aggregate_calls"}) {
		$$option_hash{"subtasks"}{"aggregateCallsBySample"} = 1;
	}
	# Events
	if ($$option_hash{files}{"target_event_files"}) {
		$$option_hash{"subtasks"}{"getTargetEventFiles"} = 1;
	}
	if ($$option_hash{files}{reference_call_files}) {
		$$option_hash{"subtasks"}{"getEventReferenceOccurrences"} = 1;
	}
	# Variant callers
	# if ($$option_hash{files}{variant_caller_configuration_file}) {
		# $$option_hash{"subtasks"}{"configureVariantCallers"} = 1;
	# }
	# Impression
	if($$option_hash{"task"} eq "structuralVariantAnnotation" || $$option_hash{"task"} eq "breakpointAnnotation"){
		$$option_hash{"subtasks"}{"printAnnotatedBreakPoints"} = 1;
	}
}

sub testSubtasksAndOptions {
	my ($option_hash) = @_;
	# Outputs
	# If the data have been agregated, it is no more possible to output them in raw order
	if ($$option_hash{"subtasks"}{"aggregateCallsBySample"}) {
		if (defined($$option_hash{treated_options}{impression_logic}) && $$option_hash{treated_options}{impression_logic} eq "raw_order") {
			die(Errors::rawOrderImpossibleIfCallAggregation());
		}
		$$option_hash{treated_options}{impression_logic} = "by_sample";
	}

}

# Set all options with default values if no argument/conf setted them before
sub configureAllOptions {

	my $option_hash = $_[0];

	# Value generalities
	# Round
	$$option_hash{"computation"}{"round"} = defined($$option_hash{raw_options}{round}) ? $$option_hash{raw_options}{round} : 1;
	# Control
	$$option_hash{"computation"}{"value_control"} = defined($$option_hash{treated_options}{value_control}) ? $$option_hash{treated_options}{value_control} :"signal";
	
	# Input generalities
	$$option_hash{"inputs"}{"interprete_name"} = defined($$option_hash{"treated_options"}{"interpret_input_file_names"}) ? $$option_hash{"treated_options"}{"interpret_input_file_names"} : 0;

	# Samples
	if (defined($$option_hash{treated_options}{target_samples}[0])) {
		@{ $$option_hash{"samples"}{"targets"} } = @{ $$option_hash{treated_options}{target_samples} };
	}
	
	# General consideration of a breakpoint
	# Breakpoints
	$$option_hash{"breakpoints"}{"distance"} = defined($$option_hash{"raw_options"}{"maximum_breakpoint_distance"}) ? $$option_hash{"raw_options"}{"maximum_breakpoint_distance"} : 0;
	$$option_hash{"breakpoints"}{"clustered"} = 0;
	$$option_hash{"breakpoints"}{"annotation"}{"bed"}{"considered_element"} = "closest";
	$$option_hash{"breakpoints"}{"formatting"}{"impression_order"} = "original"; # The breakpoints will be printed in the same order they have been given # TODO: print them by chromosome and by position
	$$option_hash{"vcf_parsing"}{"bnd_extension"} = defined($$option_hash{treated_options}{"bnd_extension"}) ? $$option_hash{treated_options}{"bnd_extension"} : "1";
	$$option_hash{"vcf_parsing"}{"bnd_extension_start"} = defined($$option_hash{raw_options}{"bnd_extension_start"}) ? $$option_hash{raw_options}{"bnd_extension_start"} : "CIPOS";
	$$option_hash{"vcf_parsing"}{"bnd_extension_end"} = defined($$option_hash{raw_options}{"bnd_extension_end"}) ? $$option_hash{raw_options}{"bnd_extension_end"} : "CIEND";
	# TODO : max event size can limit transitivity
	# $$option_hash{"breakpoints"}{"max_size"} = 1000; # The size of a bp, from first to second boundary
	# $$option_hash{"breakpoints"}{"incorrect_size_decision"} = "exclusion"; # The breakpoints with incorrect sizes will be discarded or not
	# TODO : I suppose that the last option means that if there is a max size for a breakpoint, the inputs that exceed this size can :
	# - be deleted
	# - led to error
	# - be cut
	$$option_hash{"calls"}{"aggregate"} = defined($$option_hash{treated_options}{"aggregate_calls"}) ? $$option_hash{treated_options}{"aggregate_calls"} : 0;

	# SV and CNV relations
	$$option_hash{"sv_cnv"} = "separate";

	# CNV
	$$option_hash{"cnv"}{"overlap_threshold"} = 0.8;

	# SV
	$$option_hash{"sv"}{"transitivity"} = 0;
	$$option_hash{"sv"}{"clustering"}{"consider_event"} = 1;
	$$option_hash{"sv"}{"printing"}{"print_at_each_bp"} = 1;

	# Complex events
	$$option_hash{"complex_events"}{"current_number"} = 0;

	# Events
	$$option_hash{"events"}{"interpretation"} = defined($$option_hash{treated_options}{interprete_events}) ? $$option_hash{treated_options}{interprete_events} : 1;
	$$option_hash{"events"}{"formatting"}{"impression_order"} = "sorted";
	$$option_hash{"events"}{"incorrect_event_type_decision"} = "exclusion"; # The event with unknown event type will be discarded or not
	$$option_hash{"events"}{"cluster_different_event_type"} = "no"; # A deletion and a duplication cannot be considered as the same event
	$$option_hash{"events"}{"filters"}{"maximum_occurrences"} = defined($$option_hash{raw_options}{event_maximum_occurrence}) ? $$option_hash{raw_options}{event_maximum_occurrence} : undef;
	$$option_hash{"events"}{"filters"}{"maximum_frequency"} = defined($$option_hash{raw_options}{event_maximum_frequency}) ? $$option_hash{raw_options}{event_maximum_frequency} : undef;
	$$option_hash{"events"}{"ignore_bnd_orientation"} = defined($$option_hash{treated_options}{ignore_bp_orientation}) ? $$option_hash{treated_options}{ignore_bp_orientation} : 0;
	$$option_hash{"events"}{"ignore_event_type"} = defined($$option_hash{treated_options}{ignore_event_type}) ? $$option_hash{treated_options}{ignore_event_type} : 0;
	$$option_hash{"events"}{"merge_bnd"} = defined($$option_hash{treated_options}{force_bnd_aggregation}) ? $$option_hash{treated_options}{force_bnd_aggregation} : 0;


	# OUTPUTS
	# Breakpoints are printed by default, but tasks will lead to print calls or events
	$$option_hash{"impression"}{"targetted_entities"}[0] = "breakpoints";
	# Folder
	$$option_hash{"impression"}{"repertory"} = $$option_hash{"raw_options"}{"output_directory"} ?  $$option_hash{"raw_options"}{"output_directory"} : ".";
	$$option_hash{"impression"}{"repertory"} =~ s/\/$//; # We file the potential
	# Printed files
	$$option_hash{"impression"}{"suffixes"} = defined($$option_hash{raw_options}{output_suffix}) ? $$option_hash{raw_options}{output_suffix} : undef;
	$$option_hash{"impression"}{"impression_logic"} = defined($$option_hash{treated_options}{impression_logic}) ? $$option_hash{treated_options}{impression_logic} : "raw_files";
	$$option_hash{"impression"}{"existent_files"} = defined($$option_hash{treated_options}{existent_output_files}) ? $$option_hash{treated_options}{existent_output_files} : "error";
	# If the options did not specify the impression options, we determinate them directly from the data
	# Other options
	$$option_hash{"version"} = "0.1.0";
	$$option_hash{"verbose_level"} = "1";

}

sub configurationAfterInputAcquisition {
	my ($option_hash, $data_hash) = @_;
	testOptionCoherenceAfterInputAcquisition($option_hash, $data_hash);
	refineTasksFromInputs($option_hash, $data_hash);
	refineOptionsFromInputs($option_hash, $data_hash);
}

sub testOptionCoherenceAfterInputAcquisition {
	my ($option_hash, $data_hash) = @_;
	# We control that all the samples seen in the different inputs are consistent with the arguments, and between them
	Samples::afterInputControl($option_hash, $data_hash);
	
}

# UTILISE
sub refineTasksFromInputs {
	my ($option_hash, $data_hash) = @_;
	# Calls
	if ($$option_hash{treated_options}{aggregate_calls} && defined($$option_hash{variant_callers}{total_used}) && $$option_hash{variant_callers}{total_used} > 1 ) {
		$$option_hash{"subtasks"}{"aggregateCallsBySample"} = 1;
	}
	# Events
	if (defined($$data_hash{"sorted_samples"}{all}[0]) && @{ $$data_hash{"sorted_samples"}{all} } > 1) {
		$$option_hash{"subtasks"}{"getBreakpointOccurrences"} = 1;
	}
}

# UTILISE
sub refineOptionsFromInputs {
	my ($option_hash, $data_hash) = @_;
	print "### Refine options from inputs ###\n";

	# Based on the sample number, we convert the frequency into an occurrence
	if ($$option_hash{events}{filters}{maximum_frequency}) {
		my $corresponding_occurrence =  $$option_hash{events}{filters}{maximum_frequency}*$$option_hash{samples}{total_seen};
		if ($corresponding_occurrence < 1) {
			$corresponding_occurrence = 1; # Occurrences cannot be null
		}
		# If --emo is also used, we take the littlest of both
		if ($$option_hash{events}{filters}{maximum_occurrences}) {
			if ($$option_hash{events}{filters}{maximum_occurrences} > $corresponding_occurrence) {
				$$option_hash{events}{filters}{maximum_occurrences} = $corresponding_occurrence;
			}
		} else {
			$$option_hash{events}{filters}{maximum_occurrences} = $corresponding_occurrence;
		}
		# We ceil and do not round : if 8,7 patients are tolerated, then 8 are, but 9 are not
		$$option_hash{events}{filters}{maximum_occurrences} = ceil($corresponding_occurrence);
		print "### Maximum number of samples before filtering : ".$$option_hash{events}{filters}{maximum_occurrences}." ###\n";
	}

	print "\n";
}

# UTILISE
sub subtaskConfigurationAfterFieldDetermination {
	my ($option_hash, $data_hash) = @_;
	# If there is no printed fields concerning annotation files, there is no need to cross breakpoints and annotations
	if (!($$data_hash{"tracks_of_interest"}{"annotationFiles"})) {
		$$option_hash{subtasks}{getAnnotationsFromFiles} = 0;
		$$option_hash{subtasks}{breakpointAnnotation} = 0;
	}
	#TODO
	# If there is no printed fields concerning genes, there is no need to cross breakpoints and gene files
	if (!($$data_hash{"tracks_of_interest"}{"annotationFiles"})) {
		$$option_hash{"subtasks"}{"getGenesFromFiles"} = 0;
		$$option_hash{"subtasks"}{"geneAndBreakpointCrossing"} = 0;
	}
}
 
# TAF ?
sub verbose {
	my ($importance_level, $message, $option_hash) = @_;
	if ($importance_level >= $$option_hash{"verbose_level"}) {
		print $message;
	}
}



1;
