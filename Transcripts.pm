package Transcripts;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

# Variables
our %gff_transcripts = (
	"transcript" => 1,
	"miRNA" => 1,
	"primary_transcript" => 1,
	"lnc_RNA" => 1,
	"mRNA" => 1,
	"snoRNA" => 1,
	"antisense_RNA" => 1,
	"snRNA" => 1,
	"ncRNA" => 1,
	"rRNA" => 1,
);

# A transcript is an Interval with a name and belonging to a gene
our @ISA = ("Intervals");


# Transcript Object, an interval with a name, and other information such as exons or introns
sub new {
	my ($class, $name, $parent_gene, $chromosome, $position_5p, $position_3p) = @_;
	$class = ref($class) || $class;
	my $transcript = {};
	$transcript = $class->Intervals::new( $chromosome, $position_5p, $position_3p );
	$transcript->{"name"} = $name;
	# Parent gene treatment
	$transcript->{"parent_gene"} = $parent_gene;
	$parent_gene->{"transcripts"}{$transcript} = $transcript;
	bless($transcript, $class);
	return($transcript);
}

1;


