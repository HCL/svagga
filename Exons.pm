package Exons;

###### AUTHOR

# Copyright 2019 Pierre-Antoine (Rollat-)Farnier (pierre-antoine.rollat-farnier@chu-lyon.fr)

###### LICENCE

## This program is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. 
# If not, see <http://www.gnu.org/licenses/>

use strict;
use warnings FATAL => 'all';

# An exon is an Interval with a parent transcript
our @ISA = ("Intervals");

# Gene Object, an interval with a name, and other information such as exons or introns
sub new {
	my ($class, $parent_transcript, $chromosome, $position_5p, $position_3p) = @_;
	$class = ref($class) || $class;
	my $exon = {};
	$exon = $class->Intervals::new( $chromosome, $position_5p, $position_3p );
	$exon->{"parent_transcript"} = $parent_transcript;
	$parent_transcript->{"exons"}{$exon} = $exon;
	bless($exon, $class);
	return($exon);
}




1;
